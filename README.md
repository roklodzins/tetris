# Tetris

Requirements:
- Python 3.10 or better
- kivy 2.1.0 or better
- kivymd (last commit)
- Cython

pip install "kivy>=2.1.0"

pip install git+https://github.com/kivymd/KivyMD.git

pip install cython

