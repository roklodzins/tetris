import os

os.environ["KIVY_USE_DEFAULTCONFIG"] = "0"

from kivy.config import Config  # NO kivy IMPORT BEFORE kivy.config

Config.set('input', 'mouse', 'mouse,disable_multitouch')
Config.set('graphics', 'resizable', '0')
Config.set('graphics', 'width', '1280')
Config.set('graphics', 'height', '720')
Config.set('graphics', 'minimum_width', '1280')
Config.set('graphics', 'minimum_height', '720')
Config.set('graphics', 'maxfps', '144')
Config.set('kivy', 'exit_on_escape', '0')
Config.write()

from kivy.clock import Clock
from kivy.core.text import LabelBase
from kivy.core.window import Window
from kivy.lang import Builder
from kivymd.app import MDApp

from os.path import join, dirname

Clock.max_iteration = max(26, Clock.max_iteration)
LabelBase.register("GameFont", "assets/font.ttf")
Window.clearcolor = (.2, .2, .2, 1)


from core.controller import Input, InputManager, XinputEvent, XInputControllerManager, WiimoteEvent, WiimoteControllerManager
from core.engine import Engine, TetriminoManager, Tetrimino, Tetrimino_T, Tetrimino_O, Tetrimino_Z, Tetrimino_L, Tetrimino_I, Tetrimino_S, Tetrimino_J, CycleMatrix, Matrix, UI, LocalServer, Actions, GameOver
from core.gfx import TitleScreen, MainMenuScreen, GameConfigScreen, ResultScreen, ControllerScreen, SettingsScreen, HelpScreen, HelpMenu, AdvScreen, AdvScreenManager, TableTile, Table, AdvLabel, get_text_size, Spinbox, SpinboxOption, AdvCheckbox, AdvButton, GameFieldScreen, GameField, GameFieldMulti, GameFieldMultiScreen
from core.games import Game, GameResult, MarathonGame, MarathonEngine, MultiLocalEngine, MultiLocalGame, UltraGame, UltraEngine, SprintGame, SprintEngine
from core.misc import IncreasePriority, TileSet, TileSetAccess, PlayerSave, sec2time, TSpin, LockdownMode, Abstract, Timer, EndCondition


__all__ = ('Input', 'InputManager', 'XinputEvent', 'XInputControllerManager', 'WiimoteEvent', 'WiimoteControllerManager',
		   'Engine', 'TetriminoManager', 'Tetrimino', 'Tetrimino_T', 'Tetrimino_O', 'Tetrimino_Z', 'Tetrimino_L', 'Tetrimino_I', 'Tetrimino_S', 'Tetrimino_J', 'CycleMatrix', 'Matrix', 'UI', 'LocalServer', 'Actions', 'GameOver',
		   'TitleScreen', 'MainMenuScreen', 'GameConfigScreen', 'ResultScreen', 'ControllerScreen', 'SettingsScreen', 'HelpScreen', 'HelpMenu', 'AdvScreen', 'AdvScreenManager', 'TableTile', 'Table', 'AdvLabel', 'get_text_size', 'Spinbox', 'SpinboxOption', 'AdvCheckbox', 'AdvButton', 'GameFieldScreen', 'GameField', 'GameFieldMulti', 'GameFieldMultiScreen',
		   'Game', 'GameResult', 'MarathonGame', 'MarathonEngine', 'MultiLocalEngine', 'MultiLocalGame', 'UltraGame', 'UltraEngine', 'SprintGame', 'SprintEngine',
		   'IncreasePriority', 'TileSet', 'TileSetAccess', 'PlayerSave', 'sec2time', 'TSpin', 'LockdownMode', 'Abstract', 'Timer', 'EndCondition')


class TetrisApp(MDApp):

	@staticmethod
	def on_key_down(k, key, c, mod):
		scancode, keyrepr = key
		if keyrepr == "f11": Window.fullscreen = not Window.fullscreen
		else: InputManager.dispatch_keydown(keyrepr, "KEYBOARD")

	@staticmethod
	def on_key_up(k, key):
		scancode, keyrepr = key
		if keyrepr != "f11": InputManager.dispatch_keyup(keyrepr, "KEYBOARD")

	@staticmethod
	def to_window(*_): return 0, 0  # avoid crash on Window move due to cls.Keyboard

	def build(self):
		self.icon = "assets/logo.png"
		Window.maximize()
		Window.fullscreen = True
		Window.request_keyboard(lambda *x: None,
		                        self).bind(on_key_down = self.on_key_down,
		                                   on_key_up = self.on_key_up)

		IncreasePriority()
		self.theme_cls.primary_palette = "Teal"
		self.theme_cls.accent_palette = "Teal"

		return Builder.load_file(join(dirname(__file__), "gfx/main.kv"))

	def on_start(self):
		if XInputControllerManager.is_available():
			Clock.schedule_interval(XInputControllerManager.loop, 0)

		if WiimoteControllerManager.is_available():
			WiimoteControllerManager.search()
			Clock.schedule_interval(WiimoteControllerManager.loop, 0)

		Clock.schedule_interval(lambda x: setattr(self, "title", f"Tetris            {round(Clock.get_fps(), 1)} fps"), .5)


	def open_settings(self): pass  # Disable Kivy Config Panel on F1 press
