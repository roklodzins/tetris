import os

HIGH_PRIORITY_CLASS = 0x0080
NORMAL_PRIORITY_CLASS = 0x0020
REALTIME_PRIORITY_CLASS = 0x0100


__all__ = "IncreasePriority",


if os.name == "nt":
	from ctypes import windll, c_bool, c_uint
	from os import getpid

	def IncreasePriority():
		process = windll.kernel32.OpenProcess(c_uint(0x0200 | 0x0400), c_bool(False), c_uint(getpid()))

		if windll.kernel32.GetPriorityClass(process) != REALTIME_PRIORITY_CLASS:
			try: return windll.kernel32.SetPriorityClass(process, c_uint(REALTIME_PRIORITY_CLASS)) != 0
			finally: windll.kernel32.CloseHandle(process)
		return False
else:
	def IncreasePriority(): pass
