from abc import ABC
from enum import IntEnum
from typing import Callable

from kivy.clock import Clock


__all__ = "sec2time", "TSpin", "LockdownMode", "Abstract", "Timer", "EndCondition"


def sec2time(sec: int) -> str:
	"""Convert an int to the format hh:mm:ss

	Args:
		sec (int): second in range [0, 359999]

	Returns (str): a string as hh:mm:ss (between 00:00:00 and 99:59:59)"""

	sec = min(max(0, sec), 359999)
	return f"{sec // 3600:02}:{sec // 60 % 60:02}:{sec % 60:02}"


class TSpin(IntEnum):
	NoTSpin = 0
	TSpinMini = 1
	TSpin = 2


class LockdownMode(IntEnum):
	InfinitePlacement = 0
	ExtendedPlacement = 1
	Classic = -1


class Abstract(ABC):
	__slots__ = ()

	def __new__(cls):
		raise RuntimeError("Cannot instanciate an abstract class")


class Debug:
	__slots__ = ()
	refs = {}

	def __new__(cls, *x):
		dc = cls
		while dc.__bases__[-1] != Debug: dc = dc.__bases__[-1]
		if dc not in cls.refs:	cls.refs[dc] = []
		o = super().__new__(cls)
		import weakref

		def free(x):
			if x in cls.refs[dc]:
				cls.refs[dc].remove(x)
			print("COLLECT", dc.__qualname__, x)

		w = weakref.ref(o, free)
		cls.refs[dc].append(w)

		print(">>> THERE IS", len(cls.refs[dc]), dc.__qualname__ , f"({cls.__qualname__})")

		return o


class Timer:
	__slots__ = "callback", "_paused", "_interval", "_event", "_clock", "__weakref__"

	def __init__(self, callback: Callable, interval: float, repeat: bool = False):
		"""Initialize a Timer object used to delay the call of a specified callback which can be repeated.

		Args:
			callback (Callable): the callback called after the delay
			interval (float): the time interval after which the callback is called
			repeat (bool): if True, the Timer is restarted after the callback call."""

		self._paused = False
		self.callback = callback
		self._interval = interval
		self._clock = lambda c, i: Clock.schedule_interval(c, i) if repeat else Clock.schedule_once(c, i)
		self._event = self._clock(self._func, interval)
		self._event.cancel()

	@property
	def is_started(self) -> bool:
		return self._event.is_triggered

	@property
	def current_interval(self) -> float:
		return self._event.timeout

	@property
	def interval(self) -> float:
		return self._interval

	@interval.setter
	def interval(self, interval: float):
		if interval == self._interval: return
		if self.pause():
			self._event.timeout = self._event.timeout / self._interval * interval
			self.start()
		else: self._event.timeout = interval
		self._interval = interval

	def start(self):
		self._event()

	def stop(self) -> bool:
		if n := self._event.is_triggered:
			self._event.cancel()

		if self._paused:
			self._event.timeout = self.interval
			self._paused = False

		return n

	def pause(self) -> bool:
		if n := self.is_started:
			T = self._event._last_dt
			self._event.cancel()
			self._event()
			self._event.cancel()
			self._event.timeout = max(self._event.timeout - (self._event._last_dt - T), 0)
			self._paused = True
		return n

	def _func(self, _):
		self.callback()
		if self._paused:
			self._event.timeout = self.interval
			self._paused = False

	def __del__(self):
		if self._event:
			self._event.cancel()


class EndCondition(IntEnum):
	Continue = -1
	GameOver = 0
	Victory = 1
