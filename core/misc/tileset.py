from core.misc.save import PlayerSave

from kivy.event import EventDispatcher
from kivy.graphics.texture import Texture
from kivy.properties import AliasProperty, ObjectProperty, StringProperty
from kivy.uix.image import Image
from os.path import isfile, isdir, join
from os import listdir


__all__ = "TileSet", "TileSetAccess"


class LayeredFS:

	def __init__(self, *paths):
		self.paths = paths

	def find(self, file: str):
		yield from (f for i in self.paths if isfile(f := join(i, file)))

	def load_image(self, file: str) -> Image:
		for i in self.find(file):
			try: return Image(source = i).texture
			except: continue


class BaseTileSet(EventDispatcher):
	Ghost_Num = 8
	Mino_Void_Num    = 0
	Ghost_Red_Num    = Ghost_Num - 7
	Ghost_Blue_Num   = Ghost_Num - 6
	Ghost_Cyan_Num   = Ghost_Num - 5
	Ghost_Green_Num  = Ghost_Num - 4
	Ghost_Orange_Num = Ghost_Num - 3
	Ghost_Purple_Num = Ghost_Num - 2
	Ghost_Yellow_Num = Ghost_Num - 1
	Ghost_Dark_Num   = Ghost_Num - 0
	Mino_Red_Num     = Ghost_Num + 1
	Mino_Blue_Num    = Ghost_Num + 2
	Mino_Cyan_Num    = Ghost_Num + 3
	Mino_Green_Num   = Ghost_Num + 4
	Mino_Orange_Num  = Ghost_Num + 5
	Mino_Purple_Num  = Ghost_Num + 6
	Mino_Yellow_Num  = Ghost_Num + 7
	Mino_Dark_Num    = Ghost_Num + 8

	theme = StringProperty(PlayerSave.theme)

	assets = StringProperty("assets/")
	ui_path = AliasProperty(lambda s: f"{s.assets}ui/", bind = ["assets"])
	theme_path = AliasProperty(lambda s: f"{s.assets}theme/", bind = ["assets"])

	loader = ObjectProperty(None)

	Mino_Cell = ObjectProperty(None)
	Mino_Void = ObjectProperty(None)

	Ghost_Red    = ObjectProperty(None)
	Ghost_Blue   = ObjectProperty(None)
	Ghost_Cyan   = ObjectProperty(None)
	Ghost_Green  = ObjectProperty(None)
	Ghost_Orange = ObjectProperty(None)
	Ghost_Purple = ObjectProperty(None)
	Ghost_Yellow = ObjectProperty(None)
	Ghost_Dark   = ObjectProperty(None)

	Mino_Red    = ObjectProperty(None)
	Mino_Blue   = ObjectProperty(None)
	Mino_Cyan   = ObjectProperty(None)
	Mino_Green  = ObjectProperty(None)
	Mino_Orange = ObjectProperty(None)
	Mino_Purple = ObjectProperty(None)
	Mino_Yellow = ObjectProperty(None)
	Mino_Dark   = ObjectProperty(None)

	Logo = ObjectProperty(None)
	Banner = ObjectProperty(None)

	UI_Glass = ObjectProperty(None)
	UI_SlashedGlass = ObjectProperty(None)
	UI_HalfGlass_0  = ObjectProperty(None)
	UI_HalfGlass_1  = ObjectProperty(None)

	UI_HelpMatrix_00 = ObjectProperty(None)
	UI_HelpMatrix_01 = ObjectProperty(None)
	UI_HelpTetrimino = ObjectProperty(None)

	UI_HelpWiimote = ObjectProperty(None)
	UI_HelpXinput  = ObjectProperty(None)

	Wiimote = ObjectProperty(None)
	Controller = ObjectProperty(None)
	Landscape = ObjectProperty(None)
	Cockpit = ObjectProperty(None)

	__slots__ = ()

	def __init__(self):
		super().__init__()
		self.bind(assets = self.new_loader, ui_path = self.new_loader,
		          theme_path = self.new_loader, theme = self.new_loader,
		          loader = self.load)
		self.new_loader()

	def new_loader(self, *_):
		self.loader = LayeredFS(f"{self.theme_path}{self.theme}/", f"{self.theme_path}Default/", self.ui_path, self.assets)

	def load(self, *_):
		self.Ghost_Red    = self.loader.load_image("s_r.png")
		self.Ghost_Blue   = self.loader.load_image("s_b.png")
		self.Ghost_Cyan   = self.loader.load_image("s_c.png")
		self.Ghost_Green  = self.loader.load_image("s_g.png")
		self.Ghost_Orange = self.loader.load_image("s_o.png")
		self.Ghost_Purple = self.loader.load_image("s_p.png")
		self.Ghost_Yellow = self.loader.load_image("s_y.png")
		self.Ghost_Dark   = self.loader.load_image("s_d.png")

		self.Mino_Red     = self.loader.load_image("c_r.png")
		self.Mino_Blue    = self.loader.load_image("c_b.png")
		self.Mino_Cyan    = self.loader.load_image("c_c.png")
		self.Mino_Green   = self.loader.load_image("c_g.png")
		self.Mino_Orange  = self.loader.load_image("c_o.png")
		self.Mino_Purple  = self.loader.load_image("c_p.png")
		self.Mino_Yellow  = self.loader.load_image("c_y.png")
		self.Mino_Dark    = self.loader.load_image("c_d.png")
		self.Mino_Cell    = self.loader.load_image("c_v.png")

		self.Mino_Void = Image(texture = Texture.create(self.Mino_Cell.size)).texture

		self.Logo   = self.loader.load_image("logo.png")
		self.Banner = self.loader.load_image("logo_banner.png")

		self.UI_Glass = self.loader.load_image("glass.png")
		self.UI_SlashedGlass = self.loader.load_image("slashed_glass.png")

		self.UI_HalfGlass_0 = self.loader.load_image("half_glass_0.png")
		self.UI_HalfGlass_1 = self.loader.load_image("half_glass_1.png")

		self.UI_HelpMatrix_00 = self.loader.load_image("help_matrix_00.png")
		self.UI_HelpMatrix_01 = self.loader.load_image("help_matrix_01.png")
		self.UI_HelpTetrimino = self.loader.load_image("help_tetriminos.png")

		self.UI_HelpWiimote = self.loader.load_image("help_wiimote.png")
		self.UI_HelpXinput  = self.loader.load_image("help_xinput.png")

		self.Controller = self.loader.load_image("controller.png")
		self.Wiimote    = self.loader.load_image("wiimote.png")
		self.Landscape  = self.loader.load_image("landscape.png")
		self.Cockpit    = self.loader.load_image("cockpit.png")


	def __getitem__(self, item: int) -> Texture:
		match item:
			case self.Mino_Void_Num:    return self.Mino_Void
			case self.Ghost_Red_Num:    return self.Ghost_Red
			case self.Ghost_Blue_Num:   return self.Ghost_Blue
			case self.Ghost_Cyan_Num:   return self.Ghost_Cyan
			case self.Ghost_Green_Num:  return self.Ghost_Green
			case self.Ghost_Orange_Num: return self.Ghost_Orange
			case self.Ghost_Purple_Num: return self.Ghost_Purple
			case self.Ghost_Yellow_Num: return self.Ghost_Yellow
			case self.Ghost_Dark_Num:   return self.Ghost_Dark
			case self.Mino_Red_Num:     return self.Mino_Red
			case self.Mino_Blue_Num:    return self.Mino_Blue
			case self.Mino_Cyan_Num:    return self.Mino_Cyan
			case self.Mino_Green_Num:   return self.Mino_Green
			case self.Mino_Orange_Num:  return self.Mino_Orange
			case self.Mino_Purple_Num:  return self.Mino_Purple
			case self.Mino_Yellow_Num:  return self.Mino_Yellow
			case self.Mino_Dark_Num:    return self.Mino_Dark
		raise ValueError(f"'{item}'")

	@classmethod
	def ghost2mino(cls, code: int): return code + cls.Ghost_Num

	@classmethod
	def mino2ghost(cls, code: int): return code - cls.Ghost_Num

	def get_themes(self) -> list[str]:
		return sorted(set(i for i in listdir(self.theme_path) if isdir(join(self.theme_path, i))) | {"Default"})

	def get_current_theme(self) -> str:
		return PlayerSave.theme if PlayerSave.theme in self.get_themes() else "Default"


TileSet = BaseTileSet()


class TileSetAccess(EventDispatcher):
	tileset = ObjectProperty(TileSet)
