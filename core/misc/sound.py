from kivy.animation import Animation
from kivy.core.audio import SoundLoader, Sound


__all__ = "Snd", "Sounds"


class Snd:
	"""Stores a sound"""

	__slots__ = "sound", "ambient", "maxvolume", "_fade_anim"

	def __init__(self, file: str = None, ambient: bool = False, maxvolume: float | int = 1):
		"""
		Args:
			file (str): the filename to load
			ambient (bool): must be True if it is and ambient music, False if just a sound effect
			maxvolume (float | int): default sound's volume, in range [0.0, 1.0]
		"""

		if file:
			self.sound: Sound | None = SoundLoader.load("assets/sound/" + file)
			self.sound.loop = ambient
			self.sound.volume = maxvolume
		else: self.sound: Sound | None = None
		self.ambient = ambient
		self.maxvolume = maxvolume
		self._fade_anim = None

	def __repr__(self) -> str: return f"<Sound {self.sound.source if self.sound else None}>"

	def play(self) -> bool:
		"""Returns (bool): True if the sound is successfully playing, False else (already playing or no sound to play)"""

		if n := (self.sound and (self.sound.state != 'play' or not self.ambient)):
			self.stop()
			if self._fade_anim:
				self._fade_anim.cancel(self)
				self._fade_anim = None

			self.sound.volume = self.maxvolume
			self.sound.play()
		return n

	def stop(self, fade: float = 0) -> bool:
		"""Stops a playing sound, with a fade in second if necessary

		Args:
			fade (float): length in second of the fade effect

		Returns (bool): True if the sound was successfully stopped (was playing before)
		"""

		if n := (self.sound and self.sound.state == 'play'):
			if self._fade_anim:
				self._fade_anim.cancel(self)
				self._fade_anim = None

			if fade:
				def oc(*_):
					if self.sound.state == 'play':
						self.sound.stop()
					self._fade_anim = None

				self._fade_anim = Animation(volume = 0, duration = fade, t = "in_out_cubic")
				self._fade_anim.bind(on_complete = oc)
				self._fade_anim.start(self.sound)
			else: self.sound.stop()
		return n


class Sounds:
	"""Collection of sound"""

	TetriminoRotate   = Snd("tetrimino_rotate.mp3")
	TetriminoMove     = Snd("tetrimino_move.mp3")
	TetriminoFail     = Snd("tetrimino_fail.mp3")  # when can move/rotate
	TetriminoHold     = Snd("tetrimino_hold.mp3")
	TetriminoHoldFail = Snd("tetrimino_hold_fail.mp3")
	TetriminoLockDown = Snd("tetrimino_lockdown.mp3")

	LineClear1 = Snd("lineclear1.mp3")
	LineClear2 = Snd("lineclear2.mp3")
	LineClear3 = Snd("lineclear3.mp3")
	LineClear4 = Snd("lineclear4.mp3")

	TSpinMini0 = Snd("tspinmini0.mp3")
	TSpinMini1 = Snd("tspinmini1.mp3")
	TSpinMini2 = Snd("tspinmini2.mp3")

	TSpin0 = Snd("tspin0.mp3")
	TSpin1 = Snd("tspin1.mp3")
	TSpin2 = Snd("tspin2.mp3")
	TSpin3 = Snd("tspin3.mp3")

	LevelUp      = Snd("levelup.mp3")
	Back2Back    = Snd("back2back.mp3")
	PerfectClear = Snd("perfectclear.mp3")

	GameOver = Snd("gameover.mp3")
	Victory  = Snd("victory.mp3")

	GameTheme = Snd("game_theme.mp3", True)
	MenuTheme = Snd("menu_theme.mp3", True)

	@classmethod
	def get_all(cls) -> list[Snd]: return [u for i in dir(cls) if isinstance(u := getattr(cls, i), Snd)]

	@classmethod
	def play(cls, sound: Snd):
		if sound.play() and sound.ambient:
			for i in cls.get_all():
				if i is not sound: i.stop(0.8)

	@classmethod
	def stop(cls, sound: Snd, fade: float | int = 0):
		sound.stop(fade)
