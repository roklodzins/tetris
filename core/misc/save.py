from core.misc.utils import LockdownMode

from datetime import datetime
from types import UnionType
from typing import Callable
from kivy.core.window import Keyboard

import json


__all__ = "PlayerSave",


class SavedProperty:
	__slots__ = "value",

	def get(self) -> object:
		return self.value

	def set(self, value: object):
		self.value = value


class SavedObject(SavedProperty):
	__slots__ = "vtype", "norm", "check"

	def __init__(self, vtype: type | UnionType, value: object,
	             norm: Callable[[object], object] = lambda x: x,
	             check: Callable[[object], bool] = lambda x: True):

		if not isinstance(value, vtype): raise TypeError(f"value '{value}' must be instance of '{vtype}'")
		self.norm = norm
		self.check = check
		self.vtype = vtype
		self.set(value)

	def set(self, value: object):
		if isinstance(value, self.vtype):
			if self.check(value := self.norm(value)):
				self.value = value


class SavedDict(SavedProperty):
	__slots__ = "strict", "norm", "check"

	def __init__(self, value: dict, strict: bool = False,
	             norm: Callable[[object], object] = lambda x: x,
	             check: Callable[[object], bool] = lambda x: True):

		if not isinstance(value, dict): raise TypeError(f"value '{value}' must be a 'dict'")
		self.norm = norm
		self.check = check
		self.strict = strict
		self.value = value

	def set(self, value: dict):
		if isinstance(value, dict):
			for i, u in value.items():
				if isinstance(i, str) and (not self.strict or i in self.value):
					if self.check(u := self.norm(u)):
						self.value[i] = u

	@staticmethod
	def norm_result(x: object) -> list:
		m = []
		if isinstance(x, list):
			for i in x:
				if isinstance(i, dict) and all(isinstance(u, str) for u in i):
					m.append(i)
		return m

	@staticmethod
	def check_result(x: object) -> bool:
		return isinstance(x, list) and all(isinstance(i, dict) and all(isinstance(u, str) for u in i) for i in x)


class PropertyRead(type):
	def __getattribute__(cls, item: str) -> object:
		v = super().__getattribute__(item)
		return v.get() if isinstance(v, SavedProperty) else v

	def __setattr__(cls, item: str, value: object):
		v = super().__getattribute__(item)
		if isinstance(v, SavedProperty): v.set(value)
		else: super().__setattr__(item, value)


class PlayerSave(metaclass = PropertyRead):
	savefile = "save.json"
	loaded: bool = False

	theme:       str = SavedObject(str, "Default")
	player_name: str = SavedObject(str, "Joueur")

	is_hold_allowed:  bool = SavedObject(bool, True)
	is_ghost_allowed: bool = SavedObject(bool, True)

	auto_repeat_delay:   float = SavedObject(float, 0.05, norm = lambda x: max(min(round(x, 2), 0.1), 0.02), check = lambda x: 0.02 <= x <= 0.1)
	auto_repeat_trigger: float = SavedObject(float, 0.19, norm = lambda x: max(min(round(x, 2), 0.4), 0.02), check = lambda x: 0.02 <= x <= 0.4)

	lockdown_mode: LockdownMode = SavedObject(int | LockdownMode, LockdownMode.ExtendedPlacement,
	                                          norm = lambda x: {int(i): i for i in LockdownMode}.get(x, LockdownMode.ExtendedPlacement) if not isinstance(x, LockdownMode) else x,
	                                          check = lambda x: isinstance(x, int))

	keyboard_map: dict[str, str] = SavedDict({"pause"        : "escape",
							                  "hold"         : "rshift",
							                  "move_left"    : "left",
							                  "move_right"   : "right",
							                  "soft_drop"    : "down",
							                  "hard_drop"    : "up",
							                  "rotate_clock" : "enter",
							                  "rotate_aclock": "backspace"},
					                         True, check = lambda x: x in Keyboard.keycodes)

	results: dict[str, list] = SavedDict({},
	                                     norm = SavedDict.norm_result,
	                                     check = SavedDict.check_result)

	profil_data = SavedDict({"lineclear"       : 0,
			                 "victory"         : 0,
			                 "played_games"    : 0,
			                 "playtime"        : 0,
			                 "tetrimino_played": 0,
			                 "tetris"          : 0,
			                 "t-spin"          : 0,
			                 "t-spin-1"        : 0,
			                 "t-spin-2"        : 0,
			                 "t-spin-3"        : 0,
			                 "m-t-spin"        : 0,
			                 "m-t-spin-1"      : 0,
			                 "m-t-spin-2"      : 0,
			                 "perfect-clear"   : 0},
	                        True, check = lambda x: isinstance(x, int))

	@classmethod
	def is_classic_lockdown_mode(cls) -> bool:
		return cls.lockdown_mode == LockdownMode.Classic

	@classmethod
	def is_extended_lockdown_mode(cls) -> bool:
		return cls.lockdown_mode == LockdownMode.ExtendedPlacement

	@classmethod
	def is_infinite_lockdown_mode(cls) -> bool:
		return cls.lockdown_mode == LockdownMode.InfinitePlacement

	@classmethod
	def get_results(cls, name: str, result: dict, rank: list) -> list[list[str | int], ...]:
		ks = {"date"} | {i.key for i in rank}

		result["date"] = f"{datetime.now():%d/%m/%Y}"

		if name in cls.results:
			saved = [i for i in cls.results[name] if ks == set(i)]

			saved.append(result)

			for i in reversed(rank):
				saved.sort(key = lambda x: i.sort_key(x[i.key]), reverse = True)
		else: saved = [result]

		cls.results[name] = [i.copy() for i in saved[:9]]
		cls.save()

		if saved[0] is result:
			result["date"] = "Nouveau Record"
		else:
			result["date"] = "Maintenant"
			saved[0]["date"] = "Record\n" + saved[0]["date"]

		while len(saved) > 9:
			if saved[-1] != result: del saved[-1]
			else: del saved[-2]

		return [[i[u] for u in ["date"] + [i.key for i in rank]] for i in saved]

	@classmethod
	def save(cls) -> bool:
		"""Saves player's data

		Returns (bool): True on success, False else"""

		try:
			json.dump({i: getattr(cls, i) for i in dir(cls) if isinstance(type.__getattribute__(cls, i), SavedProperty)},
			          open(cls.savefile, "w"), indent = 4)
		except Exception: return False
		return True

	@classmethod
	def load(cls) -> bool:
		"""Loads player's data

		Returns (bool): True on success, False else"""

		try:
			for i, u in json.load(open(cls.savefile, "r")).items():
				if hasattr(cls, i) and isinstance(type.__getattribute__(cls, i), SavedProperty):
					setattr(cls, i, u)
			cls.save()
		except: cls.loaded = False
		else: cls.loaded = True
		return cls.loaded


PlayerSave.load()
