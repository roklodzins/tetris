from core.controller.input import InputManager
from core.misc import Abstract

try:
	from . import xinputlib as xi
	xi.set_deadzone(xi.DEADZONE_TRIGGER, 10)

except IOError:
	import warnings
	warnings.simplefilter("once", ImportWarning)
	warnings.warn("XInput is not available, please use Windows", ImportWarning)
	xi = None


__all__ = "XinputEvent", "XInputControllerManager"


class XinputEvent(Abstract):
	"""Represents XInput Events"""

	__slots__ = ()

	A = "XINPUT_A"
	B = "XINPUT_B"
	Y = "XINPUT_Y"
	X = "XINPUT_X"

	LEFT_THUMB = "XINPUT_LEFT_THUMB"
	RIGHT_THUMB = "XINPUT_RIGHT_THUMB"
	XINPUT_LEFT_TRIGGER  = LEFT_TRIGGER  = "XINPUT_LEFT_TRIGGER"
	XINPUT_RIGHT_TRIGGER = RIGHT_TRIGGER = "XINPUT_RIGHT_TRIGGER"
	LEFT_SHOULDER = "XINPUT_LEFT_SHOULDER"
	RIGHT_SHOULDER = "XINPUT_RIGHT_SHOULDER"

	BACK = "XINPUT_BACK"
	START = "XINPUT_START"

	DPAD_UP = "XINPUT_DPAD_UP"
	DPAD_DOWN = "XINPUT_DPAD_DOWN"
	DPAD_LEFT = "XINPUT_DPAD_LEFT"
	DPAD_RIGHT = "XINPUT_DPAD_RIGHT"

	LSTICK_UP = "XINPUT_LSTICK_UP"
	LSTICK_DOWN = "XINPUT_LSTICK_DOWN"
	LSTICK_LEFT = "XINPUT_LSTICK_LEFT"
	LSTICK_RIGHT = "XINPUT_LSTICK_RIGHT"

	RSTICK_UP = "XINPUT_RSTICK_UP"
	RSTICK_DOWN = "XINPUT_RSTICK_DOWN"
	RSTICK_LEFT = "XINPUT_RSTICK_LEFT"
	RSTICK_RIGHT = "XINPUT_RSTICK_RIGHT"

	CONNECT_STATE = "XINPUT_CONNECT_STATE"  # 'Pressed' means connected / 'Released' means disconnected


class XInputController:
	"""Represents a single XInput controller, dispatches events to InputManager"""

	__slots__ = "idx", "_LStick", "_RStick", "_button", "_connected"

	def __init__(self, idx: int):
		self._LStick: str | None = None
		self._RStick: str | None = None
		self._button: set = set()
		self.idx = f"XINPUT{idx}"

		self._connected: bool = False

	@property
	def connected(self) -> bool:
		"""Returns (bool): True if connected to a XInput controller"""

		return self._connected

	@connected.setter
	def connected(self, connected: bool):
		if self._connected != connected:
			self._connected = connected
			if connected:
				InputManager.dispatch_keydown(XinputEvent.CONNECT_STATE, self.idx)
			else:
				for i in self._button: InputManager.dispatch_keyup(i, self.idx)  # Release all if disconnected
				self._button.clear()
				self.LStick = None
				self.RStick = None
				InputManager.dispatch_keyup(XinputEvent.CONNECT_STATE, self.idx)

	@property
	def LStick(self) -> str: return self._LStick

	@LStick.setter
	def LStick(self, LStick: str):
		if LStick != self._LStick:
			if self._LStick: InputManager.dispatch_keyup(self._LStick, self.idx)
			if LStick: InputManager.dispatch_keydown(LStick, self.idx)
			self._LStick = LStick

	@property
	def RStick(self) -> str: return self._RStick

	@RStick.setter
	def RStick(self, RStick: str):
		if RStick != self._RStick:
			if self._RStick: InputManager.dispatch_keyup(self._RStick, self.idx)
			if RStick: InputManager.dispatch_keydown(RStick, self.idx)
			self._RStick = RStick

	def press(self, key: str):
		if (key := getattr(XinputEvent, key)) not in self._button:
			self._button.add(key)
			InputManager.dispatch_keydown(key, self.idx)

	def release(self, key: str):
		if (key := getattr(XinputEvent, key)) in self._button:
			self._button.remove(key)
			InputManager.dispatch_keyup(key, self.idx)


class XInputControllerManager(Abstract):
	"""The XInput controller control system"""

	__slots__ = ()

	ctr = tuple(XInputController(i) for i in range(4))

	threshold_trigger = 0.1
	threshold_stick = 0.2

	def __class_getitem__(cls, idx: int) -> XInputController: return cls.ctr[idx]

	@classmethod
	def get(cls, idx: int) -> XInputController: return cls.ctr[idx]

	@staticmethod
	def is_available() -> bool: return bool(xi)

	@classmethod
	def get_connected(cls) -> tuple[XInputController]:
		return tuple(i for i in cls.ctr if i.connected)

	@classmethod
	def num_connected(cls) -> int:
		return sum(i.connected for i in cls.ctr)

	@classmethod
	def loop(cls, *_):
		for event in xi.get_events():
			ctr = cls.ctr[event.user_index]

			match event.type:
				case xi.EVENT_CONNECTED:    ctr.connected = True
				case xi.EVENT_DISCONNECTED:	ctr.connected = False

				case xi.EVENT_STICK_MOVED:
					match event.stick:
						case xi.LEFT:
							if (event.x ** 2 + event.y ** 2) ** 0.5 > cls.threshold_stick:
								if -event.x <= event.y <= event.x:   ctr.LStick = XinputEvent.LSTICK_RIGHT
								elif event.x <= event.y <= -event.x: ctr.LStick = XinputEvent.LSTICK_LEFT
								elif -event.y <= event.x <= event.y: ctr.LStick = XinputEvent.LSTICK_UP
								elif event.y <= event.x <= -event.y: ctr.LStick = XinputEvent.LSTICK_DOWN
							else: ctr.LStick = None
						case xi.RIGHT:
							if (event.x ** 2 + event.y ** 2) ** 0.5 > cls.threshold_stick:
								if -event.x <= event.y <= event.x:   ctr.RStick = XinputEvent.RSTICK_RIGHT
								elif event.x <= event.y <= -event.x: ctr.RStick = XinputEvent.RSTICK_LEFT
								elif -event.y <= event.x <= event.y: ctr.RStick = XinputEvent.RSTICK_UP
								elif event.y <= event.x <= -event.y: ctr.RStick = XinputEvent.RSTICK_DOWN
							else: ctr.RStick = None

				case xi.EVENT_TRIGGER_MOVED:
					match event.trigger:
						case xi.LEFT:
							if event.value > cls.threshold_trigger: ctr.press(XinputEvent.LEFT_TRIGGER)
							else: ctr.release(XinputEvent.LEFT_TRIGGER)
						case xi.RIGHT:
							if event.value > cls.threshold_trigger: ctr.press(XinputEvent.RIGHT_TRIGGER)
							else: ctr.release(XinputEvent.RIGHT_TRIGGER)

				case xi.EVENT_BUTTON_PRESSED:  ctr.press(event.button)
				case xi.EVENT_BUTTON_RELEASED: ctr.release(event.button)
