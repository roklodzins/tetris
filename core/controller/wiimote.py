from core.controller.input import InputManager
from core.misc import Abstract

try:
	from core.controller import wiiuse
except Exception as e:
	import warnings
	warnings.simplefilter("once", ImportWarning)
	warnings.warn("Wiiuse is not available: no wiimote support", ImportWarning)
	wiiuse = None

from threading import Thread
from time import sleep, time
from atexit import register


__all__ = "WiimoteEvent",


class WiimoteEvent(Abstract):
	"""Represents Wiimote Events"""

	__slots__ = ()

	WII_1  = "WII_1"
	WII_2  = "WII_2"
	WII_A  = "WII_A"
	WII_B  = "WII_B"
	WII_UP = "WII_UP"

	WII_HOME   = "WII_HOME"
	WII_DOWN   = "WII_DOWN"
	WII_LEFT   = "WII_LEFT"
	WII_RIGHT  = "WII_RIGHT"
	WII_MINUS  = "WII_MINUS"
	WII_PLUS   = "WII_PLUS"
	WII_TILT_Y = "WII_TILT_Y"

	WII_IMPULSE    = "WII_IMPULSE"
	WII_TILT_LEFT  = "WII_TILT_LEFT"
	WII_TILT_RIGHT = "WII_TILT_RIGHT"

	CONNECT_STATE = "WII_CONNECT_STATE"  # 'Pressed' means connected / 'Released' means disconnected



class Orient:
	"""Used to manage Wiimote orientation"""

	def __init__(self, orient):
		self.x = self.pitch = orient.pitch
		self.y = self.roll = orient.roll
		self.z = self.yaw = orient.yaw


def norm(angle, accel):
	"""Normalize spacial data"""

	# ACCELERATION
	# Correction de signe
	accel.x = -accel.x
	accel.y = -accel.y
	accel.z = -accel.z

	# Correction de calibrage
	if -0.04 <= accel.x <= 0.04: accel.x = 0.0
	elif 0.96 <= abs(accel.x) <= 1.04: accel.x /= abs(accel.x)
	if -0.04 <= accel.y <= 0.04: accel.y = 0.0
	elif 0.96 <= abs(accel.y) <= 1.04: accel.y /= abs(accel.y)
	if -0.04 <= accel.z <= 0.04: accel.z = 0.0
	elif 0.96 <= abs(accel.z) <= 1.04: accel.z /= abs(accel.z)

	# POSITION ANGULAIRE
	# Correction d'unité
	if accel.z >= 0:
		if angle.x < 0: angle.x = - 180 - angle.x  # transposition en angle orienté trigo [-180°; 180°]
		else: angle.x = 180 - angle.x

	# Correction de signe
	angle.y = -angle.y  # sens trigo
	angle.z = -angle.z  # sens trigo



class WiimoteController:
	"""Represents a single Wiimote, dispatches events to InputManager"""

	__slots__ = ("idx", "_num", "wiimote", "_button", "_connected", "_rumble",
	             "_lr_tilt", "_tilt_y", "_smoothing", "_threshold", "_continuous",
	             "_impulse_time", "_impulse", "_impulsing")

	def __init__(self, idx: int, wiimote):
		self._button: set = set()
		self.wiimote = wiimote
		self.idx: str = f"Wiimote{idx}"
		self._num: int = idx
		self._button: set = set()
		self._lr_tilt: str | None = None
		self._tilt_y: bool = False
		self._rumble: bool = False

		self._connected: bool = False

		self._smoothing: bool = True
		self._threshold: bool = True
		self._continuous: bool = False

		self._impulse_time: float = 0
		self._impulse: bool = False
		self._impulsing: bool = False


	@property
	def connected(self) -> bool:
		"""Returns (bool): True if connected to a Wiimote"""

		return self._connected

	@connected.setter
	def connected(self, connected: bool):
		if self._connected != connected:
			self._connected = connected
			if connected:
				self._smoothing = True
				self._threshold = True
				self._continuous = False
				wiiuse.set_leds(self.wiimote, wiiuse.LED[self._num])
				wiiuse.motion_sensing(self.wiimote, True)
				InputManager.dispatch_keydown(WiimoteEvent.CONNECT_STATE, self.idx)
			else:
				wiiuse.set_leds(self.wiimote, 0)
				for i in self._button: InputManager.dispatch_keyup(i, self.idx)  # Release all if disconnected
				self._button.clear()
				self.impulsing = False
				self.rumble = False
				self.LTilt = False
				self.RTilt = False
				self.Tilt = False
				InputManager.dispatch_keyup(WiimoteEvent.CONNECT_STATE, self.idx)
				for i in range(2):  # repeat because wiiuses behave strangely
					wiiuse.disconnect(self.wiimote)
					wiiuse.disconnected(self.wiimote)

	@property
	def led(self) -> int: return self._num

	@property
	def rumble(self) -> bool: return self._rumble

	@rumble.setter
	def rumble(self, rumble: bool):
		self._rumble = rumble
		wiiuse.rumble(self.wiimote, rumble)

	@property
	def smoothing(self) -> bool: return self._smoothing

	@smoothing.setter
	def smoothing(self, smoothing: bool):
		self._smoothing = smoothing
		if smoothing: wiiuse.set_flags(self.wiimote, wiiuse.SMOOTHING, 0)
		else: wiiuse.set_flags(self.wiimote, 0, wiiuse.SMOOTHING)

	@property
	def threshold(self) -> bool: return self._threshold

	@threshold.setter
	def threshold(self, threshold: bool):
		self._threshold = threshold
		if threshold: wiiuse.set_flags(self.wiimote, wiiuse.ORIENT_THRESH, 0)
		else: wiiuse.set_flags(self.wiimote, 0, wiiuse.ORIENT_THRESH)

	@property
	def continuous(self) -> bool: return self._continuous

	@continuous.setter
	def continuous(self, continuous: bool):
		self._continuous = continuous
		if continuous: wiiuse.set_flags(self.wiimote, wiiuse.CONTINUOUS, 0)
		else: wiiuse.set_flags(self.wiimote, 0, wiiuse.CONTINUOUS)

	def press(self, key: str):
		match key:
			case "+": key = "PLUS"
			case "-": key = "MINUS"

		if (key := getattr(WiimoteEvent, f"WII_{key}")) not in self._button:
			self._button.add(key)
			InputManager.dispatch_keydown(key, self.idx)

	def release(self, key: str):
		match key:
			case "+": key = "PLUS"
			case "-": key = "MINUS"

		if (key := getattr(WiimoteEvent, f"WII_{key}")) in self._button:
			self._button.remove(key)
			InputManager.dispatch_keyup(key, self.idx)

	@property
	def LTilt(self) -> bool: return self._lr_tilt == WiimoteEvent.WII_TILT_LEFT

	@LTilt.setter
	def LTilt(self, LTilt: bool):
		if LTilt != self.LTilt:
			if LTilt:
				self.RTilt = False
				self._lr_tilt = WiimoteEvent.WII_TILT_LEFT
				InputManager.dispatch_keydown(self._lr_tilt, self.idx)
			else:
				InputManager.dispatch_keyup(self._lr_tilt, self.idx)
				self._lr_tilt = None

	@property
	def RTilt(self) -> bool: return self._lr_tilt == WiimoteEvent.WII_TILT_RIGHT

	@RTilt.setter
	def RTilt(self, RTilt: bool):
		if RTilt != self.RTilt:
			if RTilt:
				self.LTilt = False
				self._lr_tilt = WiimoteEvent.WII_TILT_RIGHT
				InputManager.dispatch_keydown(self._lr_tilt, self.idx)
			else:
				InputManager.dispatch_keyup(self._lr_tilt, self.idx)
				self._lr_tilt = None

	@property
	def Tilt(self) -> bool: return self._tilt_y

	@Tilt.setter
	def Tilt(self, Tilt: bool):
		if Tilt != self.Tilt:
			if Tilt: InputManager.dispatch_keydown(WiimoteEvent.WII_TILT_Y, self.idx)
			else: InputManager.dispatch_keyup(WiimoteEvent.WII_TILT_Y, self.idx)
			self._tilt_y = Tilt

	@property
	def impulsing(self) -> bool: return self._impulsing

	@impulsing.setter
	def impulsing(self, impulsing: bool):
		if impulsing != self.impulsing:
			if impulsing: self._impulse_time = time()
			elif self._impulse:
				InputManager.dispatch_keyup(WiimoteEvent.WII_IMPULSE, self.idx)
				self._impulse = False
			self._impulsing = impulsing
		elif impulsing and time() - self._impulse_time > 0.06 and not self._impulse:
			InputManager.dispatch_keydown(WiimoteEvent.WII_IMPULSE, self.idx)
			self._impulse = True


_wiimotes = wiiuse.init(4) if wiiuse else [None] * 4


class WiimoteControllerManager(Abstract):
	"""The Wiimotes control system"""

	_wiimotes = _wiimotes
	_connecting = False
	ctr = tuple(WiimoteController(i, _wiimotes[i]) for i in range(4))

	def __class_getitem__(cls, idx: int) -> WiimoteController: return cls.ctr[idx]

	@classmethod
	def get(cls, idx: int) -> WiimoteController: return cls.ctr[idx]

	@staticmethod
	def is_available() -> bool: return bool(wiiuse)

	@classmethod
	def get_connected(cls) -> tuple[WiimoteController]:
		return tuple(i for i in cls.ctr if i.connected)

	@classmethod
	def num_connected(cls) -> int:
		return sum(i.connected for i in cls.ctr)

	@classmethod
	def connect(cls):
		cls._connecting = True
		try:
			if cls.num_connected() < 4 and wiiuse.find(cls._wiimotes, 4, 0):
				return wiiuse.connect(cls._wiimotes, 4)
		finally: cls._connecting = False

	@classmethod
	def search(cls) -> Thread:
		def s():
			while True:
				if cls.connect(): break  # don't poll without
				sleep(5)
		(t := Thread(target = s, daemon = True)).start()
		return t

	@classmethod
	def loop(cls, *_):
		if cls._connecting: return
		while e := wiiuse.poll(cls._wiimotes, 4):
			for ctr in cls.ctr:
				wm = ctr.wiimote.contents
				if wm.state & 0x0008 == 0x0008: ctr.connected = True
				match wm._event:
					case 3:     ctr.connected = True
					case 4 | 5:	ctr.connected = False
					case 1:  # Event
						ctr.connected = True
						if wm.btns:
							for name, b in wiiuse.button.items():
								if wiiuse.is_just_pressed(wm, b):
									ctr.press(name.upper())

						if wm.btns_released:
							for name, b in wiiuse.button.items():
								if wiiuse.is_released(wm, b):
									ctr.release(name.upper())

						if wiiuse.using_acc(wm):
							orient = Orient(wm.orient)
							norm(orient, wm.gforce)

							if 20 < orient.x < 80: ctr.LTilt = True
							elif -20 > orient.x > -80: ctr.RTilt = True
							else: ctr.LTilt = ctr.RTilt = False

							ctr.Tilt = 35 < abs(orient.y) < 80

							if 2 < abs(wm.gforce.z): ctr.impulsing = True
							elif 1.3 > abs(wm.gforce.z): ctr.impulsing = False
							ctr.impulsing = ctr.impulsing
						else:
							wiiuse.motion_sensing(ctr.wiimote, True)


	@classmethod
	def clear(cls):
		for i in cls.ctr: i.connected = False


register(WiimoteControllerManager.clear)
