from core.misc import Timer

from typing import Callable
from weakref import ref


__all__ = "Input", "InputManager"


class Input:
	"""Represents a user action, triggered when meeting specified inputs conditions"""

	__slots__ = ("keys", "_paused", "_action_down", "_action_up", "repeat", "repeat_after",
	             "repeat_interval", "_opposite", "_event", "__weakref__", "controller")

	def __init__(self, *keys: str, action_down: Callable = None,
	             action_up: Callable = None, repeat_after: float = 0, repeat_interval: float = 0):
		"""
		Args:
			*keys (str): keys names used to trigger the action, None means all keys
			action_down (Callable): action triggered when key is pressed (and repeated)
			action_up (Callable): action triggered when key is released
			repeat_after (float): starts repeating the action after this delay
			repeat_interval (float): repeats util release the action, delayed by this
		"""

		self.keys = {i: False for i in keys} if keys else {None: False}
		self._action_down = action_down
		self._action_up = action_up
		self.repeat = repeat_after > 0 and repeat_interval > 0
		self.repeat_after = repeat_after
		self.repeat_interval = repeat_interval
		self._opposite: Input | None = None
		self._event: Timer | None = Timer(self._action, self.repeat_after, True) if self.repeat else None
		self._paused = False
		self.controller = None

	@property
	def paused(self) -> bool: return self._paused

	@paused.setter
	def paused(self, paused: bool): self._paused = paused


	@property
	def action_down(self) -> Callable: return (lambda *x: x) if self._paused else self._action_down

	@action_down.setter
	def action_down(self, action_down: Callable): self._action_down = action_down


	@property
	def action_up(self) -> Callable: return (lambda *x: x) if self._paused else self._action_up

	@action_up.setter
	def action_up(self, action_up: Callable): self._action_up = action_up


	def set_opposite(self, op: 'Input'):
		"""Sets another Input as an opposite, allowing efficient cohabitation and avoid conflict:
		disable the opposite if cls is activated and vice versa

		Args:
			op (Input): the opposite Input"""

		self._opposite = op
		op._opposite = self

	def keydown(self, key: str | None, controller: str | None) -> bool:
		"""Must be called when a key is pressed, triggers action if necessary

		Args:
			key (str): the pressed key
			controller (str): the controller which pressed the key

		Returns (bool): True if the action was triggered, False else"""

		if {None} == self.keys.keys(): key = None

		r = False
		if key in self.keys:  # check if concerned by this key
			if not self.keys[key]:  # check if it is virtually not registred as pressed
				if not any(self.keys.values()):  # check if another concerned key if pressed or not
					self.controller = controller
					if self.action_down: self.action_down(controller)
					if self.repeat:
						self._event.interval = self.repeat_after
						self._event.callback = self._action
						self._event.start()  # schedule the repeat
					r = True
				self.keys[key] = True  # virtually register the key as pressed
		return r

	def keyup(self, key: str | None, controller: str | None) -> bool:
		"""Must be called when a key is released, triggers action if necessary

		Args:
			key (str): the released key
			controller (str): the controller which released the key

		Returns (bool): True if the action was triggered, False else"""

		if {None} == self.keys.keys(): key = None

		if key in self.keys:  # check if concerned by this key
			if self.keys[key]:  # check if it is virtually registred as pressed
				self.keys[key] = False  # virtually register the key as not pressed
				if not any(self.keys.values()):  # check if another concerned key if pressed, if not, triggers the action_up
					if self.repeat: self._event.stop()  # stop the repeat
					self.controller = controller
					if self.action_up: self.action_up(controller)
					return True
		return False

	def _action(self, *_):
		if self.action_down: self.action_down(self.controller)
		self._event.interval = self.repeat_interval
		self._event.callback = lambda *x: self.action_down and self.action_down(self.controller) or True

	def stop(self):
		"""Stop the action trigger if activated

		Notes: call it before deleting the Input object"""

		if self._event: self._event.stop()

	def __del__(self):
		self.stop()


class InputManager:
	"""Allows the centralization of all inputs systems, manages associated actions,
	 and conflicts between opposed inputs, typically simultaneous left and right inputs"""

	_managers: list[ref, ...] = []  # weakref

	__slots__ = "keys", "inputs", "controllers", "__weakref__"

	def __new__(cls, *a, **k):
		i = super().__new__(cls)
		cls._managers.append(ref(i, lambda x: x in cls._managers and cls._managers.remove(x)))
		return i

	def __init__(self, *inputs: Input, controllers: list[str] = None):
		self.keys: list[str, ...] = []
		self.controllers = controllers or None
		self.inputs = inputs

	@classmethod
	def dispatch_keydown(cls, key: str, controller: str = None):
		for i in cls._managers:
			if n := i(): n.keydown(key, controller)

	@classmethod
	def dispatch_keyup(cls, key: str, controller: str = None):
		for i in cls._managers:
			if n := i(): n.keyup(key, controller)

	def keydown(self, key: str, controller: str = None):
		"""Must be called when a key is pressed, dispatchs it through all Inputs and manages opposite confict

		Args:
			key (str): the pressed key
			controller (str): the controller which pressed the key, None to force"""

		if (self.controllers is None or controller is None or controller in self.controllers) and key not in self.keys:
			self.keys.append(key)

			for i in self.inputs:
				if i.keydown(key, controller) and i._opposite:
					for u in i._opposite.keys:
						i._opposite.keyup(u, controller)

	def keyup(self, key: str, controller: str = None):
		"""Must be called when a key is released, dispatchs it through all Inputs and manages opposite confict

		Args:
			key (str): the released key
			controller (str): the controller which released the key, None to force"""

		if (self.controllers is None or controller is None or controller in self.controllers) and key in self.keys:
			self.keys.remove(key)
			for i in self.inputs:
				if i.keyup(key, controller) and i._opposite:
					for u in self.keys: i._opposite.keydown(u, controller)

	def pause_all(self):
		"""Pause all Input listeners"""

		for i in self.inputs: i.paused = True

	def unpause_all(self):
		"""Unpause all Input listeners"""

		for i in self.inputs: i.paused = False

	def clear(self):
		"""Clear the Input manager

		Notes: call it before deleting the InuptManager object"""

		for i in self.inputs: i.stop()
		self.inputs = ()

	def __del__(self):
		if self.inputs: self.clear()
