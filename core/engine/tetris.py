from core.controller import Input, InputManager, XinputEvent, WiimoteEvent

from core.engine.ui import UI
from core.engine.events import GameOver, Actions
from core.engine.localserver import LocalServer
from core.engine.matrix import Matrix, CycleMatrix
from core.engine.tetrimino import TetriminoManager, Tetrimino

from core.misc import EndCondition, LockdownMode, TSpin, Timer, PlayerSave, Sounds


__all__ = "Engine",


class Combo:
	"""Represents Combo level and back2back state"""

	__slots__ = "back2back", "combo_high"

	@property
	def reward_back2back(self) -> bool: return self.back2back > 1

	def __init__(self):
		self.back2back: int = 0
		self.combo_high: int = 0


class Gravity(Timer):
	"""Manage the gravity of Tetriminos"""

	__slots__ = "engine", "_softdrop"

	def __init__(self, engine):
		self.engine = engine
		self._softdrop = False
		super().__init__(self._callback,
		                 self.speed, True)

	def _callback(self):
		if self.engine.tetrimino:
			if self.engine.tetrimino.move() and self._softdrop:
				self.engine.score += 1
		self.interval = self.speed

	@property
	def speed(self) -> float:
		"""Returns (float): the current fall speed"""

		return self.engine.drop_speed if self._softdrop else self.engine.fall_speed

	@property
	def softdrop(self) -> bool:
		"""Returns (bool): True is currently softdropping"""
		return self._softdrop

	@softdrop.setter
	def softdrop(self, softdrop: bool):
		if not self._softdrop and softdrop:
			self.interval /= 20
		elif self._softdrop and not softdrop:
			self.interval *= 20

		self._softdrop = softdrop


class Engine:
	"""The main class of the game, the Engine which dispatches player action to Tetriminos instance, manages the whole game"""

	@property
	def is_destroyed(self) -> bool:
		return self._is_destroyed

	@is_destroyed.setter
	def is_destroyed(self, is_destroyed: bool):
		self._is_destroyed |= is_destroyed

	@property
	def level(self): return min(self._level, 15)

	@level.setter
	def level(self, level: int):
		init = not getattr(self, '_level', 0)
		self._level = level
		self._fall_speed = self.get_fall_speed(min(level, 15))
		self._drop_speed = self._fall_speed / 20
		if self.use_level_up:
			self.line_goal += level * 5 if self.use_variable_goal else (10 if self.cleared_lines else 10 * level)  # can be called recursively
		else:
			self.ui.LevelUpdate(self._level)

		if self.line_goal > 0:
			if not init: Sounds.play(Sounds.LevelUp)
			self.ui.LevelUpdate(self._level)


	@property
	def line_goal(self) -> int: return self._line_goal

	@line_goal.setter
	def line_goal(self, line_goal: int):
		self._line_goal = line_goal
		if self._line_goal <= 0 and self.use_level_up: self.level += 1
		else: self.ui.LineGoalUpdate(max(self._line_goal, 0))

	@property
	def cleared_lines(self) -> int: return self._cleared_lines

	@cleared_lines.setter
	def cleared_lines(self, cleared_lines: int):
		self._cleared_lines = cleared_lines
		self.ui.LinesUpdate(self._cleared_lines)

	@property
	def fall_speed(self) -> float: return self._fall_speed

	@property
	def drop_speed(self) -> float: return self._drop_speed

	@property
	def score(self): return self._score

	@score.setter
	def score(self, score: int):
		score, self._score = self._score, min(score, 999_999_999)
		self.ui.ScoreUpdate(score, self._score, self._score - score)

	@property
	def attack_column(self):
		return self._attack_column

	@attack_column.setter
	def attack_column(self, attack_column: int):
		self._attack_column = attack_column
		self.ui.AttackColumnUpdate(attack_column)

	@property
	def paused(self) -> bool: return self._paused

	@paused.setter
	def paused(self, paused: bool):
		if self.is_destroyed or not self.is_pause_allowed or paused == self._paused: return
		if paused:
			self._paused = paused
			self.gravity.pause()
			if self.tetrimino: self.tetrimino.pause()
			for i in self._timers.values(): i.pause()
			self.ui.OpenPauseMenu()
		else:
			def unpause():
				self._paused = paused
				self.gravity.start()
				if self.tetrimino: self.tetrimino.resume()
				for i in self._timers.values(): i.start()
			self.ui.ClosePauseMenu(unpause)

	@property
	def rank(self) -> int: return self._rank

	@rank.setter
	def rank(self, rank: int): self._rank = rank

	@property
	def use_level_up(self) -> bool: return self._use_level_up

	@property
	def lockdown_mode(self) -> LockdownMode: return self._lockdown_mode

	@property
	def is_pause_allowed(self) -> bool: return self._is_pause_allowed

	@property
	def is_online_mode(self) -> bool: return self._is_online

	@property
	def game_over(self) -> EndCondition: return self._game_over

	@game_over.setter
	def game_over(self, game_over: EndCondition): self._game_over = game_over

	@property
	def use_ghost(self) -> bool: return self._use_ghost

	@property
	def use_chrono(self) -> bool: return self._use_chrono

	@property
	def time(self) -> float: return self.ui.GetTime()

	@property
	def use_hold_box(self) -> bool: return self._use_hold_box

	@property
	def use_attack_column(self) -> bool: return self._use_attack_column

	@property
	def use_cycle_matrix(self) -> bool: return self._use_cycle_matrix

	@property
	def use_reversed_matrix(self) -> bool: return self._use_reversed_matrix

	@property
	def use_random_tetrimino(self) -> bool: return self._use_random_tetrimino

	@property
	def use_variable_goal(self) -> bool: return self._use_variable_goal

	@property
	def random_key(self) -> int | bytes: return self._random_key

	def setup_status(self, game):
		self._is_destroyed = False
		self._rank = 0
		self._score = 0
		self._line_goal = 0
		self._cleared_lines = 0
		self._attack_column = 0
		self._paused = False
		self._game_over            = EndCondition.Continue
		self._lockdown_mode        = game.lockdown_mode
		self._is_pause_allowed     = game.is_pause_allowed
		self._is_online            = game.is_online
		self._is_multiplayer       = game.is_multiplayer
		self._use_level_up         = game.use_level_up
		self._use_ghost            = game.is_ghost_allowed
		self._use_chrono           = game.use_chrono
		self._use_hold_box         = game.is_hold_allowed
		self._use_attack_column    = game.use_attack_column
		self._use_cycle_matrix     = game.use_cycle_matrix
		self._use_reversed_matrix  = game.use_reversed_matrix
		self._use_random_tetrimino = game.use_random_tetrimino
		self._use_variable_goal    = game.use_variable_goal
		self._random_key           = game.random_key
		self.level                 = game.starting_level
		if not self._use_level_up:
			self.line_goal        = game.line_goal or 0

	@staticmethod
	def get_fall_speed(level: int) -> float:
		"""Gets the fall speed in second of a level between 1 and 15 included"""

		return (0.8 - ((level - 1) * 0.007)) ** (level - 1)

	def __init__(self, ui: UI, game, server: LocalServer | None = None, controllers: list[str] = None):
		self.tetrimino: Tetrimino | None = None
		self.ui = ui

		self._timers = {}

		self.setup_status(server or game)
		self.server = server

		self.player_num = server.connect(self) if server else 0

		self.combo = Combo()
		self.generator = TetriminoManager(self.use_random_tetrimino, self.random_key, self.use_hold_box)

		self.gravity = Gravity(self)

		self.matrix = CycleMatrix(ui) if self.use_cycle_matrix else Matrix(ui)

		self.input = InputManager(
				Input(PlayerSave.keyboard_map["hold"], XinputEvent.X,
				      WiimoteEvent.WII_A, WiimoteEvent.WII_B,
				      action_down = self.hold),

				r := Input(PlayerSave.keyboard_map["move_right"], XinputEvent.DPAD_RIGHT,
				           XinputEvent.RIGHT_SHOULDER, XinputEvent.LSTICK_RIGHT,
				           WiimoteEvent.WII_TILT_RIGHT, WiimoteEvent.WII_DOWN,
				           repeat_after = PlayerSave.auto_repeat_trigger, repeat_interval = PlayerSave.auto_repeat_delay,
				           action_down = self.move_left if self.use_reversed_matrix else self.move_right),

				l := Input(PlayerSave.keyboard_map["move_left"], XinputEvent.DPAD_LEFT,
				           XinputEvent.LEFT_SHOULDER, XinputEvent.LSTICK_LEFT,
				           WiimoteEvent.WII_TILT_LEFT, WiimoteEvent.WII_UP,
				           repeat_after = PlayerSave.auto_repeat_trigger, repeat_interval = PlayerSave.auto_repeat_delay,
				           action_down = self.move_right if self.use_reversed_matrix else self.move_left),

				Input(PlayerSave.keyboard_map["rotate_clock"], XinputEvent.B,
				      WiimoteEvent.WII_2,
				      action_down = self.rotate_counterclockwise if self.use_reversed_matrix else self.rotate_clockwise),

				Input(PlayerSave.keyboard_map["rotate_aclock"], XinputEvent.A,
				      WiimoteEvent.WII_1,
				      action_down = self.rotate_clockwise if self.use_reversed_matrix else self.rotate_counterclockwise),

				Input(PlayerSave.keyboard_map["soft_drop"], XinputEvent.DPAD_DOWN,
				      XinputEvent.LSTICK_DOWN, XinputEvent.RIGHT_TRIGGER,
				      WiimoteEvent.WII_TILT_Y, WiimoteEvent.WII_LEFT,
				      action_down = self.hard_drop if self.use_reversed_matrix else lambda *_: setattr(self.gravity, 'softdrop', True),
				      action_up = None if self.use_reversed_matrix else lambda *_: setattr(self.gravity, 'softdrop', False)),

				Input(PlayerSave.keyboard_map["hard_drop"], XinputEvent.DPAD_UP,
				      XinputEvent.LSTICK_UP, XinputEvent.LEFT_TRIGGER,
				      XinputEvent.Y, WiimoteEvent.WII_RIGHT, WiimoteEvent.WII_IMPULSE,
				      action_down = (lambda *_: setattr(self.gravity, 'softdrop', True)) if self.use_reversed_matrix else self.hard_drop,
				      action_up = (lambda *_: setattr(self.gravity, 'softdrop', False)) if self.use_reversed_matrix else None),

				Input(PlayerSave.keyboard_map["pause"], XinputEvent.BACK, XinputEvent.START,
				      WiimoteEvent.WII_HOME, WiimoteEvent.WII_PLUS, WiimoteEvent.WII_MINUS,
				      action_down = lambda *_: setattr(self, "paused", not self.paused)),

				controllers = controllers)

		r.set_opposite(l)
		self.input.pause_all()

	def move_left(self, *_) -> bool:
		"""Moves the current Tetrimino to the left side, if available and possible

		Returns (bool): True if moved successfully, False else"""

		if self.tetrimino and not self.paused:
			if self.tetrimino.move(-1, 0):
				Sounds.play(Sounds.TetriminoMove)
				return True
			Sounds.play(Sounds.TetriminoFail)
		return False



	def move_right(self, *_) -> bool:
		"""Moves the current Tetrimino to the right side, if available and possible

		Returns (bool): True if moved successfully, False else"""

		if self.tetrimino and not self.paused:
			if self.tetrimino.move(1, 0):
				Sounds.play(Sounds.TetriminoMove)
				return True
			Sounds.play(Sounds.TetriminoFail)
		return False

	def rotate_clockwise(self, *_) -> bool:
		"""Rotates the current Tetrimino clockwise, if available and possible

		Returns (bool): True if rotated successfully, False else"""

		if self.tetrimino and not self.paused:
			if self.tetrimino.rotate(True) is not False:
				Sounds.play(Sounds.TetriminoRotate)
				return True
			Sounds.play(Sounds.TetriminoFail)
		return False

	def rotate_counterclockwise(self, *_) -> bool:
		"""Rotates the current Tetrimino counter-clockwise, if available and possible

		Returns (bool): True if rotated successfully, False else"""

		if self.tetrimino and not self.paused:
			if self.tetrimino.rotate(False) is not False:
				Sounds.play(Sounds.TetriminoRotate)
				return True
			Sounds.play(Sounds.TetriminoFail)
		return False

	def hold(self, *_) -> bool:
		"""Holds the current Tetrimino if allowed and possible

		Returns (bool): True if hold successfully, False else"""

		if self.paused or not self.tetrimino: return False
		try:
			if tetrimino := self.generator.hold(self.tetrimino):
				self.ui.HoldBoxUpdate(self.tetrimino.FACES[0])
				self.ui.NextQueueUpdate(self.generator[:6])
				self.tetrimino = tetrimino(self)
			Sounds.play(Sounds.TetriminoHold if (n := (tetrimino is not None)) else Sounds.TetriminoHoldFail)
			return n
		except GameOver:
			self.end(EndCondition.GameOver)
		return False

	def hard_drop(self, *_):
		if self.tetrimino and not self.paused: self.tetrimino.hard_drop()

	def run(self, *_):
		self.input.unpause_all()

		try:
			if self.tetrimino is not None: self.tetrimino.stop()
			self.tetrimino = self.generator()(self)
			self.ui.NextQueueUpdate(self.generator[:6])
			self.gravity.start()
		except GameOver:
			self.end(EndCondition.GameOver)

	def stop(self):
		self.input.clear()
		if self.gravity:
			self.gravity.stop()
			self.gravity.engine = None

		for i in self._timers.values(): i.stop()
		self._timers.clear()
		if self.tetrimino: self.tetrimino.stop()
		self.tetrimino = None
		self.generator = None
		self.gravity = None
		self.server = None
		self.is_destroyed = True

	def finalize(self):
		Sounds.play(Sounds.TetriminoLockDown)
		self.gravity.stop()

		lineclear = self.matrix.get_filled_lines()

		match len(lineclear), self.tetrimino.tspin:
			case 0, TSpin.TSpinMini: action = Actions.TSpinMini0
			case 0, TSpin.TSpin:     action = Actions.TSpin0
			case 1, TSpin.NoTSpin:   action = Actions.LineClear1
			case 1, TSpin.TSpinMini: action = Actions.TSpinMini1
			case 1, TSpin.TSpin:     action = Actions.TSpin1
			case 2, TSpin.NoTSpin:   action = Actions.LineClear2
			case 2, TSpin.TSpinMini: action = Actions.TSpinMini2
			case 2, TSpin.TSpin:     action = Actions.TSpin2
			case 3, TSpin.NoTSpin:   action = Actions.LineClear3
			case 3, TSpin.TSpin:     action = Actions.TSpin3
			case 4, _:               action = Actions.LineClear4
			case _:                  action = None

		if action and self.matrix.length() == action.lineclear * 10:
			action = Actions.PerfectClear

		score = 0
		lineattack = 0
		cleared_lines = 0

		if action:
			self.combo.combo_high += 1
			lineattack += action.lineattack
			score += action.score * self.level
			cleared_lines = action.awarded_lineclear if self.use_variable_goal else action.lineclear

			if n := (action.back2back is not None):
				if action.back2back: self.combo.back2back += 1
				else: self.combo.back2back = 0

				if self.combo.reward_back2back:
					score += action.score * self.level >> 1
					lineattack += 1

					if self.use_variable_goal:
						cleared_lines += action.awarded_lineclear >> 1

			b2b = n and self.combo.reward_back2back
			self.ui.RewardedAction(action.name, score, b2b)
			Sounds.play(action.sound)
			if b2b: Sounds.play(Sounds.Back2Back)

		else:
			self.combo.combo_high = 0

		self.score += score + (self.tetrimino.dist_hd << 1)
		self.cleared_lines += cleared_lines
		self.line_goal -= cleared_lines

		if lineattack and self.use_attack_column and self.server:
			self.attack_column = max(r := self.attack_column - lineattack, 0)
			if r: self.server.send_attack(self.player_num, abs(r))

		if lineclear:
			self.ui.MatrixLineClear(lineclear)
			self._timers["lineclear"] = t = Timer(lambda: self.next(lineclear), 0.3)
			t.start()
		else:
			self.next()

	def next(self, lineclear: tuple[int, ...] = ()):
		if "lineclear" in self._timers: del self._timers["lineclear"]
		if lineclear: self.matrix.line_clear(lineclear)

		if self.attack_column:
			self.matrix.line_inject(self.attack_column)
			self.attack_column = 0

		if (n := self.get_end_condition()) is not EndCondition.Continue:
			self.end(n)
		else: self.run()

	def get_end_condition(self) -> EndCondition:
		return EndCondition.Continue if self.level > 0 else EndCondition.GameOver

	def end(self, endcond: EndCondition):
		self.game_over = endcond
		if self.server:
			x = self.server.get_alive_opponent(self.player_num)
			self.server.update_state(self)
			y = self.server.get_alive_opponent(self.player_num)
			if not x or y:
				Sounds.play(Sounds.Victory if endcond == EndCondition.Victory else Sounds.GameOver)
		else:   Sounds.play(Sounds.Victory if endcond == EndCondition.Victory else Sounds.GameOver)
		self.stop()
		self.ui.End()
