from core.misc import EndCondition

import os


__all__ = "LocalServer",


class LocalServer:
	"""A server that manages engines and dispatches events to engines"""

	__slots__ = "game", "engines", "rank", "players", "random_key"


	def __init__(self, game, players: list[int]):
		"""
		Args:
			game (Game): the Game instance
			players (list[int]): a list containing, in order, players ids
		"""

		self.game = game
		self.engines = {}
		self.rank = len(players)
		self.players = players
		self.random_key = os.urandom(42) if self.game.random_key is None else self.game.random_key

	def connect(self, engine) -> int:
		"""Connects an Engine to the LocalServer

		Args:
			engine (Engine): the Engine to connect

		Returns (int): the Id of the engine"""

		if not self.players: raise RuntimeError(f"Too many engine, only {len(self.engines)} are required")
		self.engines[n := self.players.pop(0)] = engine
		return n

	def send_attack(self, player: int, lines: int):
		"""Sends line attack to other players

		Args:
			player (int): the player id that send an attack
			lines (int): the number of lines to send
		"""

		for n, e in self.engines.items():
			if n == player: continue
			e.attack_column += lines

	def get_alive_players(self) -> int:
		"""Returns (int): the number of alive players"""

		return len([e for e in self.engines.values() if e.game_over == EndCondition.Continue])

	def get_alive_opponent(self, player: int) -> int:
		"""Get the number of alive opponent

		Args:
			player (int): the referential player Id

		Returns (int): the number of alive opponent of the given player"""

		return len([i for i, e in self.engines.items()
		            if i != player and e.game_over == EndCondition.Continue])

	def update_state(self, engine):
		"""Update the state of the server from the given engine and update it in return

		Args:
			engine (Engine): the engine to update from and with
		"""

		if engine.game_over != EndCondition.Continue:
			engine.rank = self.rank
			self.rank -= 1
		f = [i for i in self.engines.values() if i.game_over == EndCondition.Continue]
		match len(f):
			case 1:	f[0].end(EndCondition.Victory)

	def __getattr__(self, item: str) -> object:
		"""Proxy on the Game"""

		return getattr(self.game, item)

	def get(self, item: str, default = None) -> object:
		"""Get an item from the server or, if missing, from the Game

		Args:
			item (str): the item's name to retrieve
			default (object): if not found, this is returned

		Returns (object): the retrieve object, found in this order: server > game > default"""

		return n if (n := getattr(self, item, k := [])) is not k else default

	def quit(self):
		"""Clear the server, used to help the gc"""

		if self.engines:
			self.engines.clear()
			self.engines = None
		self.game = None
