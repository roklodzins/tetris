from typing import Callable


class UI:
	"""Store every UI callbacks (User Interface, Input x Engine links, and GFX x Engine links) used through the Engine"""

	def MatrixUpdate(self, matrix: bytearray):
		"""Called after Matrix edition

		Args:
			matrix (bytearray | Matrix): the matrix
		"""
		print("Undefined MatrixUpdate:", matrix)

	def MatrixLineClear(self, linecleared: tuple[int, ...]):
		"""Called before clearing lines, for animation purpose

		Args:
			linecleared (tuple[int, ...]): cleared lines to animate
		"""
		print("Undefined MatrixLineClear:", linecleared)

	def TetriminoHardDrop(self, src_center: tuple[float, float],
	                      dst_center: tuple[float, float],
	                      visual_size: tuple[int, int]):
		"""Called after harddrop and before lockdown, for animation purpose

		Args:
			src_center (tuple[float, float]): center of the tetrimino before harddrop
			dst_center (tuple[float, float]): center of the tetrimino after harddrop
			visual_size (tuple[int, int]): visual size of the tetrimino rect
		"""
		print("Undefined TetriminoHardDrop:", src_center, dst_center, visual_size)

	def ScoreUpdate(self, old_score: int, score: int, increment: int):
		"""Called after a score update

		Args:
			old_score (int): score before the update
			score (int): score after the update
			increment (int): score - old_score
		"""
		print("Undefined ScoreUpdate:", old_score, score, increment)

	def LevelUpdate(self, level: int):
		"""Called after a level update

		Args:
			level (int): the new level (1 <= lvl <= 15)
		"""
		print("Undefined LevelUpdate:", level)

	def LineGoalUpdate(self, linegoal: int):
		"""Called after a linegoal update

		Args:
			linegoal (int): the new line goal
		"""
		print("Undefined LineGoalUpdate:", linegoal)

	def LinesUpdate(self, lines: int):
		"""Called after a cleared lines update

		Args:
			lines (int): the new total cleared lines number
		"""
		print("Undefined LinesUpdate:", lines)

	def AttackColumnUpdate(self, attack: int):
		"""Called before receiving an attack

		Args:
			attack (int): the number of lines which will appear after the next lockdown
		"""
		print("Undefined AttackColumnUpdate:", attack)

	def NextQueueUpdate(self, tetriminos: list):
		"""Called after a queue update

		Args:
			tetriminos (list[Tetrimino]): Tetriminos in the queue, in order
		"""
		print("Undefined NextQueueUpdate:", tetriminos)

	def HoldBoxUpdate(self, tetrimino: None | object):
		"""Called after a queue update

		Args:
			tetrimino (None | object): Tetrimino in the hold box
		"""
		print("Undefined HoldBoxUpdate:", tetrimino)

	def RewardedAction(self, action: str, score: int, b2b: bool):
		"""Called after an Action to show it to the user

		Args:
			action (str): the name of the action, showed to the user
			score (int): earned score from the action
			b2b (bool): True if back to back
		"""
		print("Undefined RewardedAction:", action, score, b2b)

	def LockDownStartAnim(self, remaining_time: int | float, coord: list[int]):
		"""Starts the Lockdown animation

		Args:
		    remaining_time (int | float): the remaining time
			coord (list[int]): linear coordinates of Minos of the current Tetrimino
		"""
		print("Undefined LockDownStartAnim:", coord)

	def LockDownStopAnim(self):
		"""Stops the Lockdown animation"""
		print("Undefined LockDownStopAnim")

	def LockDownPauseAnim(self):
		"""Pauses the Lockdown animation (because the player paused the game)"""
		print("Undefined LockDownPauseAnim")

	def LockDownMoveAnim(self, remaining_time: int | float, coord: list[int]):
		"""Moves the Lockdown animation

		Args:
		    remaining_time (int | float): the remaining time
			coord (list[int]): linear coordinates of Minos of the current Tetrimino
		"""
		print("Undefined LockDownMoveAnim:", remaining_time, coord)

	def LockDownResumeAnim(self, remaining_time: int | float):
		"""Resumes the Lockdown animation

		Args:
		    remaining_time (int | float): the remaining time
		"""
		print("Undefined LockDownResumeAnim:", remaining_time)

	def OpenPauseMenu(self):
		"""Opens the pause menu"""
		print("Undefined OpenPauseMenu")

	def ClosePauseMenu(self, cb: Callable):
		"""Closes the pause menu and unpause the engine

		Args:
			cb (Callable): callback used to unpause the engine
		"""
		print("Undefined ClosePauseMenu:", cb() or cb)

	def GetTime(self) -> int:
		"""Allow the Engine to get the ellapsed time from the begining

		Returns (int): the ellapsed time in second
		"""
		print("Undefined GetTime")
		return 0

	def End(self):
		"""Called when the game is finished"""
		print("Undefined End")
