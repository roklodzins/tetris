from core.misc import Abstract, Sounds, Snd


__all__ = ("GameOver", "Actions")


class GameOver(Exception): pass


class Action:
	"""Represents an Action realized after a Tetrimino Lockdown"""

	__slots__ = "lineattack", "lineclear", "awarded_lineclear", "score", "name", "back2back", "sound"

	def __init__(self, lineattack: int = 0, lineclear: int = 0, score: int = 0,
	             name: str = "", back2back: bool | None = None, sound: Snd = None):

		for n, v in locals().items():
			if v != self: setattr(self, n, v)

		self.awarded_lineclear = score // 100


class Actions(Abstract):
	"""Set of all possible Actions"""

	LineClear1 = Action(0, 1, 100, "Single", False, Sounds.LineClear1)
	LineClear2 = Action(1, 2, 300, "Double", False, Sounds.LineClear2)
	LineClear3 = Action(2, 3, 500, "Triple", False, Sounds.LineClear3)
	LineClear4 = Action(4, 4, 800, "Tetris", True, Sounds.LineClear4)

	TSpinMini0 = Action(0, 0, 100, "Mini T-Spin", None, Sounds.TSpinMini0)
	TSpinMini1 = Action(0, 1, 200, "Mini T-Spin Single", True, Sounds.TSpinMini1)
	TSpinMini2 = Action(2, 2, 400, "Mini T-Spin Double", True, Sounds.TSpinMini2)

	TSpin0 = Action(0, 0, 400, "T-Spin", None, Sounds.TSpin0)
	TSpin1 = Action(2, 1, 800, "T-Spin Single", True, Sounds.TSpin1)
	TSpin2 = Action(4, 2, 1200, "T-Spin Double", True, Sounds.TSpin2)
	TSpin3 = Action(6, 3, 1600, "T-Spin Triple", True, Sounds.TSpin3)

	PerfectClear = Action(8, 8, 2000, "Perfect Clear", True, Sounds.PerfectClear)

	__slots__ = ()
