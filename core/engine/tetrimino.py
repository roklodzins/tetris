from core.engine.events import GameOver
from core.misc import TileSet, LockdownMode, TSpin, Timer

from os import urandom
from random import Random


__all__ = ("Tetrimino", "Tetrimino_I", "Tetrimino_J", "Tetrimino_L", "Tetrimino_O", "Tetrimino_S",
		   "Tetrimino_T", "Tetrimino_Z", "TetriminoManager")


USUAL_WALL_KICK   = ((((0, 0), (-1, 0), (-1,  1), (0, -2), (-1, -2)), ((0, 0), (1,  0), (1,   1), (0, -2), (1,  -2))),
					 (((0, 0), (1,  0), (1,  -1), (0,  2), (1,   2)), ((0, 0), (1,  0), (1,  -1), (0,  2), (1,   2))),
					 (((0, 0), (1,  0), (1,   1), (0, -2), (1,  -2)), ((0, 0), (-1, 0), (-1,  1), (0, -2), (-1, -2))),
					 (((0, 0), (-1, 0), (-1, -1), (0,  2), (-1,  2)), ((0, 0), (-1, 0), (-1, -1), (0,  2), (-1,  2))))

SPECIAL_WALL_KICK = ((((0, 0), (-2, 0), (1,  0), (-2, -1), (1,   2)), ((0, 0), (-1, 0), (2,  0), (-1,  2), (2,  -1))),
					 (((0, 0), (-1, 0), (2,  0), (-1,  2), (2,  -1)), ((0, 0), (2,  0), (-1, 0), (2,   1), (-1, -2))),
					 (((0, 0), (2,  0), (-1, 0), (2,   1), (-1, -2)), ((0, 0), (1,  0), (-2, 0), (1,  -2), (-2,  1))),
					 (((0, 0), (1,  0), (-2, 0), (1,  -2), (-2,  1)), ((0, 0), (-2, 0), (1,  0), (-2, -1), (1,   2))))


class Tetrimino:
	"""Represents and manages a Tetrimino"""

	GHOST: int = None  # image of the ghost mino
	COLOR: int = None  # image of the mino
	FACES: list[list[int]] | tuple[tuple[tuple[int, ...], ...], ...] = None  # shape of the North face of the Tetrimino / shapes of all faces (after __init_subclass__)

	FACES_COORD = None  # created when subclassing
	CENTERS = None      # created when subclassing

	WALL_KICK = USUAL_WALL_KICK
	ALLOW_TSPIN: bool = False  # allow T-Spin (only for Tetrimino_T)

	DIM: int = 3

	__slots__ = ("X", "Y", "min_low_y", "index", "engine", "matrix", "tspin", "last_success_action",
				 "current_ghost_coord", "current_coord", "move_allowed", "rotate_allowed", "dist_hd", "dist_sd",
				 "__weakref__", "was_touch_ground", "last_low_y",
				 "lockdown_timer", "locking_down", "_lockdown_counter", "lockdown_pause_y", "lockdown_mode", "locked_down")

	@classmethod
	def generate_random(cls, rng: Random) -> type:
		"""Returns a random Tetrimino subclass

		Args:
			rng (Random): a Random object from TetriminoManager

		Returns (type): a Tetrimino subclass with random color and mino number and order"""

		block = [1] * (n := rng.randint(1, 7)) + [0] * (9 - n)
		rng.shuffle(block)

		class Tetrimino_R(cls):
			COLOR = rng.randint(TileSet.Mino_Red_Num, TileSet.Mino_Yellow_Num)
			GHOST = TileSet.mino2ghost(COLOR)
			FACES = (block[:3],
					 block[3:6],
					 block[6:])

			__slots__ = ()

		return Tetrimino_R

	def __init_subclass__(cls):
		if cls.FACES:
			cls.DIM = len(cls.FACES)
			cls.FACES = (n := tuple(tuple(u and cls.COLOR for u in i) for i in cls.FACES)), *(n := tuple(map(tuple, map(reversed, zip(*n)))) for _ in range(3))
			cls.FACES_COORD = tuple(tuple((i, u) for i in range(cls.DIM) for u in range(cls.DIM) if s[i][u]) for s in cls.FACES)
			cls.CENTERS = tuple(tuple(sum(x) / len(f) + 0.5 for x in zip(*f)) for f in cls.FACES_COORD)

	def __init__(self, engine):
		self.engine = engine
		self.matrix = self.engine.matrix

		self.X = 3 if self.DIM > 2 else 4
		self.Y = 22 - self.DIM

		self.index = 0
		self.dist_hd = 0
		self.dist_sd = 0

		self.locked_down = False
		self.locking_down = False
		self.lockdown_pause_y = 40
		self._lockdown_counter = 15
		self.lockdown_mode = self.engine.lockdown_mode
		self.lockdown_timer = Timer(self._locking_down_timer, 0.5, False)

		self.tspin = TSpin.NoTSpin
		self.last_success_action: int | bool = False

		self.was_touch_ground = False
		self.last_low_y = 40

		self.current_ghost_coord = set()  # Stores global coords of all Ghost Minos of the Ghost Tetrimino
		self.current_coord = set()  # Stores global coords of all Minos of the Tetrimino


		self.move_allowed:   tuple[dict, ...] = ({}, {}, {}, {})
		self.rotate_allowed: tuple[dict, ...] = ({}, {}, {}, {})

		for i, u in self.FACES_COORD[0]:
			if self.matrix.is_used(self.X + i, self.Y + u):
				raise GameOver("Block Out")  # Can not spawn : Block Out GameOver

		self._move()  # spawn the Tetrimino & the ghost in the matrix

		self.min_low_y = self.low_y

		if self.is_touch_ground: self.start_lockdown()

		self.move()  # must drop 1 row if possible (see guideline)

	# --------------------------------- Lock-Down ---------------------------------------

	def _locking_down_timer(self, *_):
		"""Executed at the end of the lockdown countdown, marks (and performs if possible) the lockdown"""
		self.locking_down = True
		if self.is_touch_ground:
			self.lockdown()

	def reset_counter(self):
		"""Resets the reset counter"""

		self._lockdown_counter = 15

	def decrease_counter(self):
		"""Decrease the reset counter, once this counter reach 0, it is no longer possible to reset the lockdown timer"""

		self._lockdown_counter -= self.lockdown_mode  # -1 if Extended, 0 if Infinite

	def start_lockdown(self) -> bool:
		"""Starts the lockdown timer

		Returns (bool): True if the timer was successfully started, False else (means already started)"""

		if r := not self.lockdown_timer.is_started:
			if self.low_y < self.lockdown_pause_y:
				self.lockdown_timer.interval = 0.5
				self.lockdown_pause_y = 40
			self.engine.ui.LockDownStartAnim(self.lockdown_timer.current_interval, [self.matrix.vec_to_1Dcoord(i) for i in self.current_coord])
			self.lockdown_timer.start()
		return r

	def stop_lockdown(self) -> bool:
		"""Stops the lockdown timer

		Returns (bool): True if the timer was successfully stopped, False else (means already stopped)"""

		if r := self.lockdown_timer.stop():
			self.engine.ui.LockDownStopAnim()
		return r

	def reset_lockdown(self) -> bool:
		"""Resets the timer to 0.5s and restarts it

		Returns (bool): True if the timer was successfully reset, False else"""

		if r := (self._lockdown_counter > 0):
			self.stop_lockdown()
			self.lockdown_pause_y = 40
			self.start_lockdown()
			self.locking_down = False
		return r

	def pause_lockdown(self) -> bool:
		"""Pauses the lockdown timer, the next start will resume it

		Returns (bool): True if the timer was successfully paused, False else (means timer not running)"""

		if r := self.lockdown_timer.pause():
			self.engine.ui.LockDownStopAnim()
			self.lockdown_pause_y = self.last_low_y
		return r

	def move_lockdown(self):
		"""Moves the lockdown animation"""

		if self.lockdown_timer.pause():
			self.engine.ui.LockDownMoveAnim(self.lockdown_timer.current_interval,
			                                [self.matrix.vec_to_1Dcoord(i) for i in self.current_coord])
			self.lockdown_timer.start()

	def pause(self):
		"""Pauses the Tetrimino"""

		if self.lockdown_timer.pause():
			self.engine.ui.LockDownPauseAnim()

	def resume(self):
		"""Resumes the paused Tetrimino"""

		if self.is_touch_ground:
			self.engine.ui.LockDownResumeAnim(self.lockdown_timer.current_interval)
			self.lockdown_timer.start()


	@property
	def is_touch_ground(self) -> bool:
		"""Returns (bool): True if a move down is not allowed"""

		return self.current_ghost_coord == set()  # not cls.can_move()

	@property
	def center(self) -> tuple[float, float]:
		"""Returns (tuple[float, float]): the coord of the visual center of the Tetrimino"""

		return (n := self.CENTERS[self.index])[0] + self.X, n[1] + self.Y

	@property
	def visual_size(self) -> tuple[int, int]:
		"""Returns (tuple[float, float]): the minimal size of a rectangle containing the Tetrimino
		ex: Tetrimino_I's North Face: (1, 4)"""

		mx =  max(self.current_coord, key = lambda x: x[0])[0] + 1
		mx -= min(self.current_coord, key = lambda x: x[0])[0]
		my =  max(self.current_coord, key = lambda x: x[1])[1] + 1
		my -= min(self.current_coord, key = lambda x: x[1])[1]
		return mx, my

	@property
	def low_y(self): return min(self.current_coord, key = lambda x: x[1])[1]

	def get_ghost_coord(self) -> int:
		"""Returns (int): the Y coord of the Ghost Tetrimino"""

		i = -1
		while self.can_move(y = i): i -= 1
		return self.Y + i + 1

	def can_move(self, x: int = 0, y: int = -1) -> bool:
		"""Looks if a move can be performed, the result is cached for faster later access

		Args:
			x (int): relative x coord
			y (int): relative y coord

		Returns (bool): True if the move can be performed, False else"""

		if not self: return False
		if (n := (D := self.move_allowed[self.index]).get((self.X + x, self.Y + y))) is not None: return n

		if any(filter(lambda x: self.matrix.is_used(*x), {(i + x + self.X, u + y + self.Y) for i, u in self} - self.current_coord)):
			D[self.X + x, self.Y + y] = False
			return False

		D[self.X + x, self.Y + y] = True
		return True

	def can_rotate(self, clockwise: bool = True) -> bool | int:
		"""Looks if a rotation can be performed

		Args:
			clockwise (bool): rotate clockwise if True, counter-clockwise else

		Returns (bool | int): the index n (int) in the WALL_KICK table used if the rotation can be performed, False else"""

		if not self: return False
		if self.WALL_KICK is None: return 0

		if (n := (D := self.rotate_allowed[self.index]).get((self.X, self.Y, clockwise))) is not None: return n

		f = self.FACES_COORD[(self.index + (clockwise or -1)) % 4]
		for n, (kx, ky) in enumerate(self.WALL_KICK[self.index][not clockwise]):
			for i, u in f:
				i += kx + self.X
				u += ky + self.Y
				if self.matrix.is_used(i, u) and (i, u) not in self: break
			else:
				D[self.X, self.Y, clockwise] = n
				return n

		D[self.X, self.Y, clockwise] = False
		return False

	def move(self, x: int = 0, y: int = -1) -> bool:
		"""Moves the Tetrimono if possible

		Args:
			x (int): relative x coord
			y (int): relative y coord

		Returns (bool): True if the move was performed successfully, False else"""

		if not self.can_move(x, y): return False

		with self:
			self.was_touch_ground = self.is_touch_ground
			self.last_low_y = self.low_y

			self.X += x
			self.Y += y

			self._move()

			self.last_success_action = True

		return True

	def rotate(self, clockwise: bool = True) -> bool | int:
		"""Rotates the Tetrimino if possible

		Args:
			clockwise (bool): rotate clockwise if True, counter-clockwise else

		Returns (bool | int): the index n (int) in the WALL_KICK table used for the rotation, False if rotation not possible"""

		if (k := self.can_rotate(clockwise)) is False: return False  # rotation impossible

		with self:
			self.was_touch_ground = self.is_touch_ground
			self.last_low_y = self.low_y

			if self.WALL_KICK:  # for Tetrimino_O
				kx, ky = self.WALL_KICK[self.index][not clockwise][k]

				self.X += kx
				self.Y += ky

			self.last_success_action = k
			self.index = (self.index + (clockwise or -1)) % 4

			self._move()

		return k

	def _move(self):
		"""Performs the move/rotate by updating the matrix"""

		with self.matrix:

			nc = {(self.X + i, self.Y + u) for i, u in self}

			for i in nc - self.current_coord: self.matrix[i] = self.COLOR
			for i in self.current_coord - nc: del self.matrix[i]

			self.current_coord = nc

			y = self.get_ghost_coord()  # override y
			nc = {(self.X + i, y + u) for i, u in self} - self.current_coord  # override nc

			if self.engine.use_ghost:
				for i in nc - self.current_ghost_coord: self.matrix[i] = self.GHOST
				for i in self.current_ghost_coord - nc - self.current_coord: del self.matrix[i]

			self.current_ghost_coord = nc

	def hard_drop(self):
		"""Performs a Hard Drop, calls the corresponding animation (TetriminoHardDrop) and lockdown the Tetrimino"""

		c = self.center
		y = self.get_ghost_coord() - self.Y
		self.dist_hd = -y
		if y: self.move(0, y)  # y > 0: avoid TSpin rotate erase
		self.engine.ui.TetriminoHardDrop(c, self.center, self.visual_size)
		self.lockdown()

	def lockdown(self):
		"""Locks down the Tetrimino, deallowing any futher action, alse computes the presence of a TSpin if eligible"""

		self.locked_down = True
		self.lockdown_timer.stop()
		self.engine.ui.LockDownStopAnim()

		if self.ALLOW_TSPIN and not isinstance(self.last_success_action, bool):  # last action was a rotation
			Tslot = (self.matrix.is_used(self.X, self.Y + 2), self.matrix.is_used(self.X + 2, self.Y + 2),
					 self.matrix.is_used(self.X, self.Y), self.matrix.is_used(self.X + 2, self.Y))

			match self.index:
				case 0:	A, B, C, D = Tslot
				case 1: C, A, D, B = Tslot
				case 2: D, C, B, A = Tslot
				case 3: B, D, A, C = Tslot
				case _: raise ValueError()

			if C and D and (A or B): self.tspin = TSpin.TSpin if self.last_success_action == 4 else TSpin.TSpinMini
			if A and B and (C or D): self.tspin = TSpin.TSpin

		self.engine.finalize()

	def stop(self):
		"""Force the lockdown

		Notes: call this before safely deleting the Tetrimino object"""

		self.last_success_action = False  # Avoid TSpin
		self.locked_down = True
		self.lockdown_timer.stop()
		self.engine = None
		self.matrix = None

	def clear(self):
		"""Clears this Tetrimino from the Matrix and lockdown"""

		for i in self.current_coord | self.current_ghost_coord: del self.matrix[i]
		self.engine.ui.LockDownStopAnim()
		self.stop()

	def __enter__(self):
		if self.lockdown_timer.is_started or self.locking_down:
			self.decrease_counter()

	def __exit__(self, *x):
		"""Checks if the lockdown timer must be started, stopped, reset or paused"""

		if (Y := self.low_y) < self.min_low_y:
			self.min_low_y = Y
			if self.lockdown_mode == LockdownMode.Classic:
				if self.lockdown_timer.is_started or self.locking_down:
					self.reset_lockdown()
			else:
				self.reset_counter()

		if self.lockdown_timer.is_started or self.locking_down:
			if self.is_touch_ground:
				if self.lockdown_mode != LockdownMode.Classic and self.reset_lockdown(): pass
				else: self.move_lockdown()

			elif not isinstance(self.last_success_action, bool) and self.was_touch_ground:  # rotate only
				self.pause_lockdown()
			else:
				self.stop_lockdown()

		elif self.is_touch_ground:
			if self._lockdown_counter < 1: self.lockdown()
			else: self.start_lockdown()

	def __bool__(self) -> bool:
		"""Returns (bool): True if the Tetrimino is NOT locked down, False else"""

		return not self.locked_down

	def __contains__(self, item: tuple[int, int]) -> bool:
		"""Tests if a cell is used by the Tetrimino

		Args:
			item (tuple[int, int]): the cell coord

		Returns (bool): True if the Tetrimino has a mino in the specified cell"""

		return item in self.current_coord

	def __iter__(self) -> iter:
		"""Iters on used coords with the current face

		Returns (iter): an iteration on FACES_COORD[index]"""

		return iter(self.FACES_COORD[self.index])

	def __del__(self):
		self.stop()



class Tetrimino_T(Tetrimino):
	GHOST = TileSet.Ghost_Purple_Num
	COLOR = TileSet.Mino_Purple_Num
	FACES = ((0, 1, 0),
			 (0, 1, 1),
			 (0, 1, 0))

	ALLOW_TSPIN = True
	__slots__ = ()


class Tetrimino_L(Tetrimino):
	GHOST = TileSet.Ghost_Orange_Num
	COLOR = TileSet.Mino_Orange_Num
	FACES = ((0, 1, 0),
			 (0, 1, 0),
			 (0, 1, 1))

	__slots__ = ()


class Tetrimino_J(Tetrimino):
	GHOST = TileSet.Ghost_Blue_Num
	COLOR = TileSet.Mino_Blue_Num
	FACES = ((0, 1, 1),
			 (0, 1, 0),
			 (0, 1, 0))

	__slots__ = ()


class Tetrimino_S(Tetrimino):
	GHOST = TileSet.Ghost_Green_Num
	COLOR = TileSet.Mino_Green_Num
	FACES = ((0, 1, 0),
			 (0, 1, 1),
			 (0, 0, 1))

	__slots__ = ()


class Tetrimino_Z(Tetrimino):
	GHOST = TileSet.Ghost_Red_Num
	COLOR = TileSet.Mino_Red_Num
	FACES = ((0, 0, 1),
			 (0, 1, 1),
			 (0, 1, 0))

	__slots__ = ()


class Tetrimino_I(Tetrimino):
	GHOST = TileSet.Ghost_Cyan_Num
	COLOR = TileSet.Mino_Cyan_Num
	FACES = ((0, 0, 1, 0),) * 4

	WALL_KICK = SPECIAL_WALL_KICK
	__slots__ = ()


class Tetrimino_O(Tetrimino):
	GHOST = TileSet.Ghost_Yellow_Num
	COLOR = TileSet.Mino_Yellow_Num
	FACES = ((1, 1),) * 2

	WALL_KICK = None
	__slots__ = ()


class TetriminoManager:
	"""A Tetriminos Manager used to:
	- get Tetriminos in a random order through a 7-bag random system
	- hold a Tetrimino"""

	__slots__ = "_tetriminos", "_rng", "_bag", "_hold", "_can_hold", "_hold_allowed"

	@property
	def can_hold(self): return self._can_hold

	@property
	def hold_tetrimino(self): return self._hold

	def __init__(self, rand: bool = False, key = None, hold_allowed: bool = True):
		"""Initialize a 7-bag Tetriminos generator

		Args:
			rand (bool): if True, just set the seed of Tetrimino_Random to key, else, initialize the bag
			key: seed of the RNG"""

		self._hold_allowed = hold_allowed
		self._can_hold = False
		if key is None: key = urandom(42)  # 42, the life answer

		self._rng = Random(key)
		self._tetriminos = None if rand else [Tetrimino_T, Tetrimino_L, Tetrimino_J, Tetrimino_S, Tetrimino_Z, Tetrimino_I, Tetrimino_O]
		self._bag = []
		self._hold = None

	def hold(self, tetrimino: Tetrimino) -> type | None:
		"""Helds a Tetrimino if possible and allowed

		Returns (type | None): the new Tetrimino type if hold successfully, None else"""

		if self._can_hold:
			tetrimino.clear()
			h, self._hold = self._hold, type(tetrimino)
			if not h: h = self()
			self._can_hold = False
			return h

	def __call__(self) -> type:
		"""Returns (type): a random Tetrimino type from the 7-bag and removes it from the 7-bag"""

		self._can_hold = self._hold_allowed
		if not self._bag: self._fill()
		return self._bag.pop(0)

	def __getitem__(self, idx: int | slice) -> type | list[type]:
		"""Returns Tetriminos types from the 7-bag

		Args:
			idx (int | slice): an int >= 0 or a slice

		Returns (type | list[type]): a Tetrimino type or a list of Tetrimino types"""

		if isinstance(idx, int):
			while idx >= len(self._bag): self._fill()
			return self._bag[idx]  # type

		while (idx.start or 0, idx.stop, idx.step or 1) != idx.indices(len(self._bag)):
			self._fill()
		return self._bag[idx]  # list[type]

	def _fill(self):
		if self._tetriminos:
			self._rng.shuffle(self._tetriminos)
			self._bag.extend(self._tetriminos)
		else:
			self._bag.append(Tetrimino.generate_random(self._rng))
