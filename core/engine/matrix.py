from core.engine.ui import UI
from core.misc import TileSet


import random

__all__ = "Matrix", "CycleMatrix"


class Matrix(bytearray):
	MCC = tuple((x, y) for x in range(9, -1, -1) for y in range(39, -1, -1))  # Matrix Coord. Cache

	__slots__ = "_hole_count", "_hole_x", "ui"

	def __init__(self, ui: UI):
		super().__init__(400)  # 0x0y, 1x0y, 2x0y, ... 0x39y, 1x39y, ... 9x39y
		self.ui = ui
		self._hole_count, self._hole_x = 0, random.randint(0, 9)

	@staticmethod
	def vec_to_1Dcoord(coord: tuple[int, int]):
		return coord[1] * 10 + coord[0] % 10

	def __setitem__(self, coord: tuple[int, int], mino: int): super().__setitem__(coord[1] * 10 + coord[0], mino)
	def __getitem__(self, coord: tuple[int, int]):     return super().__getitem__(coord[1] * 10 + coord[0])
	def __delitem__(self, coord: tuple[int, int]):     return super().__setitem__(coord[1] * 10 + coord[0], TileSet.Mino_Void_Num)

	def clear(self):
		"""Clears all the matrix and update"""

		super().__init__(400)
		self.ui.MatrixUpdate(self)

	def is_empty(self) -> bool:
		"""Returns (bool): True if the matrix is empty, False else"""

		return all(i <= TileSet.Ghost_Num for i in self)

	def length(self) -> int:
		"""Returns (int): the number of solid mino in the matrix (no ghost)"""

		return sum(i > TileSet.Ghost_Num for i in self)

	def is_used(self, x: int = 0, y: int = 0) -> bool:
		"""Gets if the specified cell is used or empty

		Args:
			x (int): the x coord of the target cell, -1 < x < 10
			y (int): the y coord of the target cell, -1 < y < 40

		Returns (bool): True if the cell is used or if x or y are invalid, else False"""
		return not -1 < x < 10 or not -1 < y < 40 or super().__getitem__(y * 10 + x) > TileSet.Ghost_Num

	def is_line_filled(self, y: int = 0) -> bool:
		"""Gets if the specified line is filled or not

		Args:
			y (int): the line index, -1 < y < 40 (no check)

		Returns (bool): True if the line y is filled, False else"""

		g = super().__getitem__
		return all(g(x) > TileSet.Ghost_Num for x in range(y * 10, y * 10 + 10))

	def export(self) -> bytes:
		"""Exports the matrix as a bytes sequence

		Returns (bytes): the sequence length is 400 bytes (10x * 40y)"""

		return bytes(self)

	def load(self, matrix: bytes):
		"""Loads the matrix from a bytes sequence and update

		Args:
			matrix (bytes): a bytes sequence from .export()"""

		super().__init__(matrix)
		self.ui.MatrixUpdate(self)

	def line_inject(self, line: int = 1) -> bool:
		"""Injects dark lines with 1 hole at the bottom of the matrix and update

		Args:
			line (int): number of lines to add (0 < x)

		Returns (bool): True if a TopOut Gameover happened, False else"""

		if line < 1: return False
		TOP_OUT_GAMEOVER = line > 40

		for i, u in self.MCC:
			if u + line > 39:
				if self.is_used(i, u):  # non-void cell out of the matrix
					TOP_OUT_GAMEOVER = True
					del self[i, u]
			else: self[i, u + line] = self[i, u]  # move lines up

		for u in range(min(line, 40)):  # inject dark lines
			self._hole_count += 1  # the hole is at the same place during 8 lines
			for i in range(10): self[i, u] = TileSet.Mino_Void_Num if i == self._hole_x else TileSet.Mino_Dark_Num
			if self._hole_count > 7: self._hole_count, self._hole_x = 0, random.randint(0, 9)

		self.ui.MatrixUpdate(self)
		return TOP_OUT_GAMEOVER

	def line_clear(self, lineclear: tuple[int, ...]) -> int:
		"""Clears specified lines and move down others and update

		Args:
			lineclear (tuple[int]): a sorted (ascending) tuple containing index (-1 < x < 40) of lines to clear

		Returns (int): number of lines cleared"""

		if not lineclear: return 0

		for i in lineclear:
			for u in range(10): del self[u, i]

		# cls.ui.MatrixLineClear(cls, lineclear)

		lineclear = list(lineclear) + [40]
		for i in range(r := len(lineclear) - 1):
			for n in range(lineclear[i] + 1, lineclear[i + 1]):
				for u in range(10): self[u, n - i - 1] = self[u, n]
			for u in range(10): del self[u, 39 - i]

		self.ui.MatrixUpdate(self)
		return r

	def get_filled_lines(self) -> tuple:
		"""Gets filled lines' coord in ascending order

		Returns (tuple): the lines' coord in ascending order"""

		return tuple(y for y in range(40) if self.is_line_filled(y))

	def get_first_line(self) -> int:
		"""Gets the first non-empty line's coord, starting from the upper one

		Returns (int): the coord of the line, -1 if the matrix empty"""

		for y in range(39, -1, -1):
			if any(self.is_used(x, y) for x in range(10)): return y
		return -1

	def __enter__(self):  # allows with statement
		pass

	def __exit__(self, *x):
		self.ui.MatrixUpdate(self)


class CycleMatrix(Matrix):
	__slots__ = ()

	def __setitem__(self, coord: tuple[int, int], mino: int): bytearray.__setitem__(self, coord[1] * 10 + coord[0] % 10, mino)
	def __getitem__(self, coord: tuple[int, int]):     return bytearray.__getitem__(self, coord[1] * 10 + coord[0] % 10)
	def __delitem__(self, coord: tuple[int, int]):     return bytearray.__setitem__(self, coord[1] * 10 + coord[0] % 10, TileSet.Mino_Void_Num)

	def is_used(self, x: int = 0, y: int = 0) -> bool:
		"""Gets if the specified cell is used or empty

		Args:
			x (int): the x coord of the target cell (-1 < x % 10 < 10)
			y (int): the y coord of the target cell, -1 < y < 40

		Returns (bool): True if the cell is used or if x or y are invalid, else False"""

		return not -1 < y < 40 or self[(x % 10, y)] > TileSet.Ghost_Num
