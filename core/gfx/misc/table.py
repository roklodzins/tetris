from core.gfx.misc.label import AdvLabel
from core.misc import TileSetAccess

from kivy.lang import Builder
from kivy.properties import BooleanProperty, ListProperty, NumericProperty, ObjectProperty
from kivy.uix.gridlayout import GridLayout


__all__ = "TableTile", "Table"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class TableTile(AdvLabel, TileSetAccess):
	light = BooleanProperty(False)


class Table(GridLayout, TileSetAccess):
	data = ListProperty([])
	orientation = "lr-tb"

	use_row_line = BooleanProperty(True)
	use_col_line = BooleanProperty(True)
	cols_size = ListProperty([])

	row_line = NumericProperty(0)
	col_line = NumericProperty(0)

	def __init__(self, rows: int = 10, cols: int = 5, data: list[list[str, ...], ...] = None, **k):
		self.cells = None

		super().__init__(rows = rows, cols = cols, **k)

		def new(i):
			self.add_widget(t := TableTile(light = i))
			return t

		self.cells: list[list[TableTile]] = [[new(i % 2) for u in range(cols)] for i in range(rows)]
		self.cells[0][0].bind(size = self._set_lines)
		if data: self.set(data)

	def on_data(self, _, d):
		self.set(d)

	def on_cols_size(self, _, c):
		d = c + [1] * self.cols
		for n, cl in enumerate(zip(*self.cells)):
			for i in cl:
				i.size_hint_x = d[n]

	def _set_lines(self, inst, size):
		self.col_line = size[0] + self.spacing[0] / 2
		self.row_line = size[1] + self.spacing[1] / 2

	def set(self, data: list[list[str, ...], ...]):
		with self:
			self.rows = len(data)
			self.cols = max(map(len, data))


			for y, line in enumerate(data):
				for x, c in enumerate(line):
					self.cells[y][x].text = str(c)

	def on_rows(self, _, rows: int):
		if self.cells is None: return

		n = len(self.cells) - rows
		if n < 0:
			for i in range(-n):
				self.cells.append([TableTile(light = bool(len(self.cells) % 2)) for u in range(self.cols)])
		else:
			for i in range(n):
				del self.cells[-1]

	def on_cols(self, _, cols: int):
		if self.cells is None: return

		n = len(self.cells[0]) - cols
		if n < 0:
			for rn, r in enumerate(self.cells):
				for i in range(-n): r.append(TableTile(light = bool(rn % 2)))
		else:
			for r in self.cells:
				del r[-n:]

	def __enter__(self):
		self.clear_widgets()

	def __exit__(self, *_):
		for i in self.cells:
			for c in i:
				self.add_widget(c)
		d = self.cols_size + [1] * self.cols
		for n, cl in enumerate(zip(*self.cells)):
			for i in cl:
				i.size_hint_x = d[n]

	def __getitem__(self, item: int) -> list[TableTile, ...]: return self.cells[item]
