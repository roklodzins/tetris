from core.gfx.misc import AdvLabel

from kivy.lang import Builder
from kivy.properties import BooleanProperty, NumericProperty, StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout

from kivymd.uix.selectioncontrol import MDCheckbox


__all__ = "AdvCheckbox",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class CheckboxLabel(ButtonBehavior, AdvLabel):
	def on_touch_down(self, touch):
		if super().on_touch_down(touch):
			touch.x = self.parent.ids.cb.center_x
			touch.y = self.parent.ids.cb.center_y
			self.parent.ids.cb.on_touch_down(touch)

	def on_touch_up(self, touch):
		if super().on_touch_up(touch):
			touch.x = self.parent.ids.cb.center_x
			touch.y = self.parent.ids.cb.center_y
			self.parent.ids.cb.on_touch_up(touch)


class AdvCheckbox_(MDCheckbox):
	norm_font_size = NumericProperty(0)

	def __int__(self, **k):
		super().__init__(**k)
		self.check_anim_in._animated_properties["font_size"] = self.norm_font_size
		self.bind(norm_font_size = self._upd_anim_font)

	def _upd_anim_font(self, _, x):
		self.check_anim_in._animated_properties["font_size"] = x


class AdvCheckbox(BoxLayout):
	text = StringProperty("")
	group = StringProperty(None, allownone = True)
	active = BooleanProperty(False)
	font_size = NumericProperty(20)

	def get(self) -> bool: return self.active
