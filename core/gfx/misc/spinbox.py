from core.misc import TileSetAccess

from kivy.lang import Builder
from kivy.properties import ListProperty, NumericProperty, ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.behaviors.focus import FocusBehavior


__all__ = "Spinbox", "SpinboxOption"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class Spinbox(FocusBehavior, BoxLayout, TileSetAccess):
	value = NumericProperty(0)
	min = NumericProperty(0)
	max = NumericProperty(100)
	increment = NumericProperty(10)
	format = ObjectProperty(str)
	font_size = NumericProperty(20)

	def get(self):
		return self.value

	def dec(self):
		self.value = max(self.value - self.increment, self.min)

	def inc(self):
		self.value = min(self.value + self.increment, self.max)

	def keyboard_on_textinput(self, w, tx):
		match tx:
			case "+": self.inc()
			case "-": self.dec()


class SpinboxOption(Spinbox):
	value = StringProperty("")
	choices = ListProperty([])

	def dec(self):
		self.value = self.choices[(self.choices.index(self.value) - 1) % len(self.choices)]

	def inc(self):
		self.value = self.choices[(self.choices.index(self.value) + 1) % len(self.choices)]
