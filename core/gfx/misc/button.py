from kivy.lang import Builder
from kivy.properties import ColorProperty
from kivymd.uix.button import MDRectangleFlatIconButton


__all__ = "AdvButton",


Builder.load_string("""
<AdvButton>:
	theme_text_color: "Custom"
	theme_icon_color: "Custom"
	text_color: self.color
	line_color: self.color
	icon_color: self.color
	size_hint_x: 1
	line_width: 1.2
""")


class AdvButton(MDRectangleFlatIconButton):
	color = ColorProperty([0, 0.698, 0.498, 1])
