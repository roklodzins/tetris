from kivy.animation import Animation
from kivy.lang import Builder
from kivy.uix.scrollview import ScrollView
from kivy.properties import AliasProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget


__all__ = "AdvSwiper"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class AdvSwiperItem(BoxLayout):
	selected = BooleanProperty(False)
	swip_duration = NumericProperty(0)

	def on_selected(self, _, s):
		Animation(size_hint_y = 1 if s else 0.8, duration = self.swip_duration).start(self)


class AdvSwiper(ScrollView):
	swip_duration = NumericProperty(0)
	items_spacing = NumericProperty(0)
	items_width = NumericProperty(100)

	current_index = ObjectProperty(0)
	current = AliasProperty(lambda s: s.get_current(), bind = ["current_index"])

	def add_widget(self, widget, *args, **kwargs):
		if not self.children:
			super().add_widget(widget, *args, **kwargs)
		else:
			self.ids.box.add_widget(a := AdvSwiperItem(swip_duration = self.swip_duration,
			                                           selected = not self.ids.box.children),
			                        *args, **kwargs)
			self.bind(swip_duration = a.setter('swip_duration'))
			a.add_widget(widget)

	def on_touch_down(self, touch):
		match touch.button:
			case "scrollup": self.next()
			case "scrolldown": self.prev()
			case "left":
				u = (self.viewport_size[0] - self.width) * self.scroll_x
				for i in self.ids.box.children:
					n = i.children[0]
					if n.x <= u + touch.x <= n.right and n.y <= touch.y - self.viewport_size[1] / 2 <= n.top:
						n.on_press()

	def on_touch_up(self, touch):
		if touch.button == "left":
			u = (self.viewport_size[0] - self.width) * self.scroll_x
			for i in self.ids.box.children:
				n = i.children[0]
				if n.x <= u + touch.x <= n.right and n.y <= touch.y - self.viewport_size[1] / 2 <= n.top:
					n.on_release()

	def get_current(self):
		return self.ids.box.children[-self.current_index - 1]

	def get_current_item(self):
		return self.get_current().children[0]

	def get(self, idx: int):
		return self.ids.box.children[-idx - 1]

	def on_current(self, _, c):
		a = self.get(0).center_x
		b = self.get(-1).center_x
		for i in self.ids.box.children: i.selected = i is c
		Animation(scroll_x = (c.center_x - a) / (b - a), duration = self.swip_duration).start(self)

	def next(self):
		if (n := self.current_index + 1) < len(self.ids.box.children):
			self.current_index = n

	def prev(self):
		if (n := self.current_index - 1) >= 0:
			self.current_index = n
