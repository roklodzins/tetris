from kivy.core.text import Label as CoreLabel
from kivy.lang import Builder
from kivymd.uix.label import MDLabel


__all__ = "AdvLabel", "get_text_size"


Builder.load_string("""
<AdvLabel>:
	halign: "center"
	theme_text_color: "Custom"
	text_color: 1, 1, 1, 1
""")


class AdvLabel(MDLabel):
	font_name = "GameFont"


def get_text_size(text: str, font_size: float | int, font_name: str = "GameFont") -> float | int:
	(lb := CoreLabel(text = text, font_size = font_size, font_name = font_name)).refresh()
	return lb.content_width
