from core.gfx.effects import BlurTransition
from core.misc import TileSetAccess, Sounds

from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import AliasProperty, NumericProperty, ObjectProperty
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition


__all__ = "AdvScreen", "AdvScreenManager"


class AdvScreenManager(ScreenManager, TileSetAccess):
	game = ObjectProperty(None, allownone = True)
	angle = NumericProperty(0)

	def __init__(self, **k):
		super().__init__(**{"transition": BlurTransition(duration = 1)} | k)
		a = Animation(angle = 5, duration = 7, t = "in_out_sine", s = 1 / 60)
		a += Animation(angle = -5, duration = 7, t = "in_out_sine", s = 1 / 60)
		a.repeat = True
		a.start(self)

	def on_angle(self, _, a):
		for i in self.children: i.angle = a


class AdvScreen(Screen, TileSetAccess):
	angle = NumericProperty(0)
	ambient_sound = ObjectProperty(Sounds.MenuTheme)

	@staticmethod
	def _get_game(self): return self.manager.game

	@staticmethod
	def _set_game(self, value):
		if value is None: raise ValueError("'NoneType' is not allowed for game")
		self.manager.game = value

	game = AliasProperty(_get_game, _set_game)

	def on_pre_enter(self):
		self.canvas.ask_update()
		for i in self.children:
			i.opacity = 0

	def on_enter(self):
		a = Animation(opacity = 1, duration = 0.3)
		for i in self.children:
			a.start(i)
		Sounds.play(self.ambient_sound)


Builder.load_string("""
<AdvScreen>:
    canvas.before:
        PushMatrix
        Translate:
            x: self.center[0]
            y: self.center[1]
        Rotate:
            angle: self.angle
            axis: 0, 0, 1
        Rectangle:
            texture: self.tileset.Landscape
            pos: -self.width * 0.55, -self.height / 6
            size: self.width * 1.1, self.width * 1.1 * self.tileset.Landscape.height / self.tileset.Landscape.width
        PopMatrix
        
        Rectangle:
            texture: self.tileset.Cockpit
            size: self.size
""")
