from core.engine import LocalServer, UI
from core.gfx.gamefield.pause import PauseMenu
from core.gfx.misc import AdvScreen
from core.misc import EndCondition, Sounds

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import AliasProperty, BooleanProperty, ObjectProperty, NumericProperty, StringProperty
from kivy.uix.anchorlayout import AnchorLayout

from typing import Callable


__all__ = "GameField", "GameFieldScreen", "GameFieldMulti", "GameFieldMultiScreen"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class GameField(AnchorLayout, UI):
	"""The graphical Gamefield, work around the engine"""

	game = AliasProperty(lambda self: self.field.game)
	field = ObjectProperty(None, allownone = True)
	engine = ObjectProperty(None, allownone = True)
	server = ObjectProperty(None, allownone = True)
	controllers = ObjectProperty(None)
	overflow = NumericProperty(1)
	poverflow = NumericProperty(1)

	def __init__(self, field, **k):
		self.field = field

		super().__init__(**k)
		self.engine = self.game.engine(self, self.game, self.server, self.controllers)

	def on_kv_post(self, base_widget):
		if self.game.use_chrono:
			self.ids.info.ids.chrono.countdown = True
			self.ids.info.ids.chrono.time = self.game.time

	def on_overflow(self, i, s):
		if s < self.poverflow and 100 / self.parent.rows not in self.size \
				and 100 / self.parent.cols not in self.size:  # filter uninitialized and downward
			self.poverflow = s

	def MatrixUpdate(self, matrix: bytearray): return self.ids.matrix.MatrixUpdate(matrix)

	def LockDownStartAnim(self, *x): return self.ids.matrix.LockDownStartAnim(*x)

	def LockDownStopAnim(self, *x): return self.ids.matrix.LockDownStopAnim(*x)

	def LockDownMoveAnim(self, *x): return self.ids.matrix.LockDownMoveAnim(*x)

	def LockDownPauseAnim(self, *x): return self.ids.matrix.LockDownPauseAnim(*x)

	def LockDownResumeAnim(self, *x): return self.ids.matrix.LockDownResumeAnim(*x)

	def TetriminoHardDrop(self, *x): return self.ids.matrix.TetriminoHardDrop(*x)

	def MatrixLineClear(self, *x): return self.ids.matrix.MatrixLineClear(*x)

	def NextQueueUpdate(self, *x): return self.ids.nextqueue.set(*x)

	def HoldBoxUpdate(self, *x): return self.ids.holdbox.set(*x)

	def ScoreUpdate(self, *x): return self.ids.info.update_score(*x)

	def AttackColumnUpdate(self, *x): return self.ids.matrix.AttackColumnUpdate(*x)

	def LevelUpdate(self, lv):
		if self.ids.info.update_level(lv) and lv > 1:
			self.ids.matrix.LevelUpdate(lv)

	def LinesUpdate(self, *x): return self.ids.info.update_lines(*x)

	def LineGoalUpdate(self, *x): return self.ids.info.update_goal(*x)

	def RewardedAction(self, *x): return self.ids.matrix.RewardedAction(*x)

	def OpenPauseMenu(self):
		self.ids.info.pause_chrono()
		self.ids.matrix.paused = True
		self.ids.holdbox.paused = True
		self.ids.nextqueue.paused = True
		self.field.OpenPauseMenu()

	def ClosePauseMenu(self, callback: Callable):
		self.engine.input.pause_all()

		def unpause(*_):
			self.ids.info.start_chrono()
			self.ids.matrix.paused = False
			self.ids.holdbox.paused = False
			self.ids.nextqueue.paused = False
			self.engine.input.unpause_all()
			callback()

		self.field.ClosePauseMenu(self)
		self.ids.matrix.Countdown(unpause)

	def Resume(self) -> bool:
		self.engine.paused = False
		return not self.engine.is_destroyed

	def Pause(self) -> bool:
		self.engine.paused = True
		return self.engine.is_destroyed

	def GetTime(self): return self.ids.info.ids.chrono.time

	def End(self):
		self.Quit(False)
		self.ids.matrix.End(f"Victoire" if self.engine.game_over == EndCondition.Victory else f"Défaite")
		if self.field:
			Clock.schedule_once(lambda *x: self.field.End(), 2.2)

	def Quit(self, free: bool = True):
		self.engine.stop()
		self.ids.info.stop_chrono()

		if free:
			self.engine = None
			self.server = None
			self.field = None

	def start(self):
		def start():
			self.ids.info.start_chrono()
			self.ids.matrix.start()
			self.ids.holdbox.start()
			self.ids.nextqueue.start()
			self.engine.run()

		self.ids.matrix.Countdown(start)


class GameFieldMulti(GameField):
	"""Variant used in multiplayers game mode"""

	player_num = NumericProperty(0)

	def __init__(self, field, player_num: int = 0, controllers: list[str] = None, **k):
		self.server = field.server
		self.player_num = player_num
		self.controllers = controllers

		super().__init__(field, **k)

	def End(self):
		self.Quit(False)
		self.ids.matrix.End(f"Victoire\n\nRang {self.engine.rank}" if self.engine.game_over == EndCondition.Victory else f"Défaite\n\nRang {self.engine.rank}")
		if self.field and not self.server.get_alive_players():
			Clock.schedule_once(lambda *x: self.field.End(), 2.2)


class GameFieldScreen(AdvScreen):
	"""Screen storing GameField"""

	name = StringProperty("gamefield_screen")
	ambient_sound = ObjectProperty(Sounds.GameTheme)
	pause_menu = ObjectProperty(None)
	paused = BooleanProperty(False)

	def End(self):
		self.manager.current = "result_screen"
		self.manager.current_screen.set_result(self.ids.grid.children[0].engine)

	def OpenPauseMenu(self):
		if self.paused: return
		self.paused = True

		self.pause_menu.open()

		if n := getattr(self, '_pause_anim', None):
			for i in self.ids.grid.children: n.cancel(i)

		self._pause_anim = Animation(opacity = 0.8, duration = 0.3)
		for i in self.ids.grid.children:
			i.Pause()
			self._pause_anim.start(i)

	def ClosePauseMenu(self, *x):
		if not self.paused: return
		self.paused = False
		self.pause_menu.dismiss()

		if n := getattr(self, '_pause_anim', None):
			for i in self.ids.grid.children: n.cancel(i)

		self._pause_anim = Animation(opacity = 1, duration = 0.3)
		for i in self.ids.grid.children:
			if i not in x: i.Resume()
			self._pause_anim.start(i)

	def Resume(self):
		for i in self.ids.grid.children:
			if i.Resume(): break

	def Quit(self):
		self.pause_menu.dismiss()
		for i in self.ids.grid.children: i.Quit(False)
		self.manager.current = "main_menu_screen"

	def Restart(self):
		self.pause_menu.dismiss()
		for i in self.ids.grid.children: i.Quit(False)
		self._restart()

	def _restart(self, *_):
		if self.manager and self.manager.current_screen is self:
			self.manager.switch_to(type(self)())

	def on_pre_enter(self):
		self.ids.grid.rows = self.ids.grid.cols = 1

		self.pause_menu = PauseMenu(quit = self.Quit, resume = self.Resume, restart = self.Restart)

		self.ids.grid.add_widget(GameField(self))
		super().on_pre_enter()

	def on_enter(self):
		super().on_enter()
		for i in self.ids.grid.children:
			i.start()

	def on_leave(self):
		super().on_leave()
		for i in self.ids.grid.children: i.Quit()
		self.manager.remove_widget(self)


class GameFieldMultiScreen(GameFieldScreen):
	"""Screen storing GameFieldMulti"""

	controllers = ObjectProperty(None)
	server = ObjectProperty(None, allownone = True)

	def __init__(self, controllers: dict[int, list[str]] = None, **k):
		self.controllers = controllers

		super().__init__(**k)

	def End(self):
		self.manager.current = "result_screen"
		self.manager.current_screen.set_multi_result(self.server)

	def on_pre_enter(self, *_):
		if not self.game.is_multiplayer: raise TypeError("Unsupported player mode")

		self.pause_menu = PauseMenu(quit = self.Quit, resume = self.Resume, restart = self.Restart)

		self.game.player_num = len(self.controllers)
		self.server = LocalServer(self.game, list(self.controllers))

		n = self.game.player_num

		r = []
		for x in range(1, n + 1):
			for y in range(1, n + 1):
				if x * y < n: continue
				r.append((x, y))
				break
		r = min(r, key = lambda x: abs(0.89 * x[0] / x[1] - self.height / self.width))

		self.ids.grid.rows, self.ids.grid.cols = r

		for num, ctr in self.controllers.items():
			self.ids.grid.add_widget(GameFieldMulti(self, num, ctr))

		super(GameFieldScreen, self).on_pre_enter()

	def _restart(self, *_):
		if self.manager and self.manager.current_screen is self:
			self.manager.switch_to(type(self)(self.controllers))

	def on_leave(self):
		super().on_leave()
		self.server.quit()
		self.server = None
