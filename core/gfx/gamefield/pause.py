from core.misc import TileSetAccess

from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.modalview import ModalView


__all__ = "PauseMenu",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class PauseMenu(BoxLayout, ModalView, TileSetAccess):
	quit = ObjectProperty(None)
	resume = ObjectProperty(None)
	restart = ObjectProperty(None)

	def pause(self):
		h = self.ids.help
		m = self.ids.menu
		h.reset()

		h.opacity = 1
		h.size_hint_x = 1

		m.opacity = 0
		m.size_hint_x = 0
		self.size_hint = .8, .8

	def unpause(self):
		h = self.ids.help
		m = self.ids.menu

		h.opacity = 0
		h.size_hint_x = 0

		m.opacity = 1
		m.size_hint_x = 1
		self.size_hint = .2, .7
