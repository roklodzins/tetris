from core.misc import Timer, sec2time, TileSetAccess
from core.gfx.misc import AdvLabel, get_text_size

from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import BooleanProperty, BoundedNumericProperty, ListProperty, NumericProperty, ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout


__all__ = "Chrono", "PlayerInfoContainer", "PlayerInfoContainerTop", "PlayerInfoLabel", "PlayerInfo", "NextQueue"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class Chrono(AdvLabel):
	countdown = BooleanProperty(False)
	time = BoundedNumericProperty(0, min = 0, max = 359999, errorhandler = lambda x: min(max(0, x), 359999))
	countdown_callback = ObjectProperty(None)

	def __init__(self, **k):
		super().__init__(**k | {"text": "00:00:00"})
		self.timer = Timer(self._time_upd, 1, True)

	def start(self):
		self.timer.start()

	def stop(self):
		self.timer.stop()

	def pause(self):
		self.timer.pause()

	def _time_upd(self):
		self.time += -self.countdown or 1
		if not self.time:
			if self.countdown and callable(self.countdown_callback):
				self.countdown_callback()
			return False

	def on_time(self, _, time):
		self.text = sec2time(time)


class PlayerInfoLabel(AdvLabel):
	def __init__(self, **k):
		super().__init__(**k)

		self.lb = AdvLabel()
		self.anim = None
		self.add_widget(self.lb)

	def blink(self, text: str):
		self.lb.text = text
		self.lb.size = self.size
		self.lb.halign = self.halign
		self.lb.center_x = self.center_x + get_text_size(self.text, self.font_size) / 2
		self.lb.center_y = self.center_y
		self.lb.opacity = 0
		self.lb.font_name = self.font_name
		self.lb.font_size = self.font_size

		if self.anim: self.anim.cancel(self.lb)
		self.anim = Animation(opacity = 1, center_x = self.lb.center_x + get_text_size(text, self.font_size) / 2,
		                      duration = 0.3) + Animation(opacity = 0, duration = 0.2)
		self.anim.start(self.lb)


class PlayerInfoContainer(BoxLayout, TileSetAccess):
	angle = NumericProperty(0)


class PlayerInfoContainerTop(BoxLayout, TileSetAccess):
	pass


class PlayerInfo(BoxLayout):
	countdown = BooleanProperty(False)
	time = BoundedNumericProperty(0, min = 0, max = 359999)

	score = NumericProperty(0)
	level = NumericProperty(0)
	lines = NumericProperty(0)
	goal = NumericProperty(0)
	game_name = StringProperty("")
	player_name = StringProperty("")
	font_size = NumericProperty(0)

	def __init__(self, **k):
		super().__init__(**k)

	def update_score(self, o, n, i):
		self.score = n
		if i: self.ids.score.blink(f"{i:+}")

	def update_level(self, x):
		i = x - self.level
		self.level = x
		if i and 1 < x < 16: self.ids.level.blink(f"{i:+}")
		return bool(i)

	def update_goal(self, x):
		self.goal = x

	def update_lines(self, x):
		i = x - self.lines
		self.lines = x
		if i: self.ids.lines.blink(f"{i:+}")

	def start_chrono(self):
		self.ids.chrono.start()

	def stop_chrono(self):
		self.ids.chrono.stop()

	def pause_chrono(self):
		self.ids.chrono.pause()


class NextQueue(BoxLayout, TileSetAccess):
	cell_size = ListProperty([0, 0])
	paused = BooleanProperty(False)

	def __init__(self, **k):
		super().__init__(**k)

	def set(self, tetriminos: tuple | list):
		n = len(tetriminos)
		for i in range(6):
			if i >= n:
				self.children[- i - 1].unset()
			else:
				self.children[- i - 1].set(tetriminos[i].FACES[0])

	def on_cell_size(self, _, size):
		for i in self.children:
			i.cell_size = size

	def on_paused(self, _, paused):
		for i in self.children:
			i.paused = paused

	def start(self):
		for i in self.children: i.start()
