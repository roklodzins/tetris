from core.misc import TileSet, TileSetAccess

from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import BooleanProperty, ListProperty
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.image import Image


__all__ = "MiniMatrixGfx", "HoldBox"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class MiniMatrixGfx(AnchorLayout):
	cell_size = ListProperty((0, 0), length = 2)
	paused = BooleanProperty(False)

	def unset(self):
		self.set()

	def set(self, matrix: list[list[int, ...], ...] = ()):
		matrix = list(map(list, matrix))

		while matrix and not any(matrix[0]): del matrix[0]  # remove empty rows (up)
		while matrix and not any(matrix[-1]): del matrix[-1]  # remove empty rows (down)

		matrix = list(zip(*matrix))

		while matrix and not any(matrix[0]): del matrix[0]  # remove empty cols (up)
		while matrix and not any(matrix[-1]): del matrix[-1]  # remove empty cols (down)

		matrix = tuple(reversed(tuple(zip(*reversed(matrix)))))

		cols = len(matrix)
		rows = cols and len(matrix[0])

		if (n := cols * rows - len(self.ids.grid.children)) < 0:
			for i in range(-n):
				self.ids.grid.remove_widget(self.ids.grid.children[0])

		self.ids.grid.cols = cols or None
		self.ids.grid.rows = rows or None

		if n > 0:
			for i in range(n):
				self.ids.grid.add_widget(m := Image(texture = TileSet.Mino_Void,
													allow_stretch = True, keep_ratio = True))

		for i in range(cols):
			for u in range(rows):
				self.ids.grid.children[i * rows + u].texture = TileSet[matrix[i][u]]

	def on_paused(self, _, paused):
		Animation(opacity = int(not paused), duration = 0.2).start(self.ids.grid)

	def start(self):
		self.ids.grid.opacity = 0
		Animation(opacity = 1, duration = 0.2).start(self.ids.grid)


class HoldBox(MiniMatrixGfx, TileSetAccess):
	pass
