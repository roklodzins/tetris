from core.misc import TileSet, TileSetAccess
from core.gfx.effects import BURST, HARDDROP_WIND, HARDDROP_WIND_REVERSED, ParticleSystem

from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import BooleanProperty, ListProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.stencilview import StencilView

from kivymd.uix.progressbar import MDProgressBar

from typing import Callable


__all__ = "MatrixGfx", "AttackColumn"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class CroppedImage(BoxLayout, StencilView):
	texture = ObjectProperty(None)

	def __init__(self, **k):
		super().__init__(size_hint = (0, 0.5))
		self.image = Image(**k, size_hint = (1, 2))
		self.add_widget(self.image)

	def on_texture(self, _, texture): self.image.texture = texture


class AttackColumn(MDProgressBar):
	activated = BooleanProperty(True)
	mparent = ObjectProperty(None)
	anim = ObjectProperty(None)

	def set(self, value: int):
		if self.anim: self.anim.cancel(self)
		self.anim = Animation(value = value, duration = 0.4)
		self.anim.start(self)

	def on_activated(self, _, value: bool):
		if value:
			if self.mparent: self.mparent.add_widget(self)
		else:
			self.mparent = self.parent
			self.parent.remove_widget(self)


class MatrixGfx(BoxLayout, TileSetAccess):
	reversed = BooleanProperty(False)
	paused = BooleanProperty(False)
	grid_pos = ListProperty((0, 0), length = 2)
	grid_size = ListProperty((0, 0), length = 2)
	cell_size = ListProperty((0, 1), length = 2)
	use_attack_column = BooleanProperty(False)

	def on_kv_post(self, base_widget):
		for i in range(210):
			im = CroppedImage if i < 10 else Image
			self.ids.bg.add_widget(im(texture = TileSet.Mino_Cell,
									  keep_ratio = True, allow_stretch = True, pos_hint = {'top': 2 - self.reversed} if i < 10 else {}))

			self.ids.grid.add_widget(im(texture = TileSet.Mino_Void,
										keep_ratio = True, allow_stretch = True, pos_hint = {'top': 2 - self.reversed} if i < 10 else {}))

		self.ids.grid.children[0].bind(size = lambda i, s: setattr(self, "cell_size", s))
		self.lockdown_anim = Animation(duration = 0.5, opacity = 0.35)
		self.hd_particle = ParticleSystem(HARDDROP_WIND_REVERSED if self.reversed else HARDDROP_WIND, update_interval = 0)
		self.add_widget(self.hd_particle)

		self.c = []
		self.particles = []
		for _ in range(21):
			self.particles.append(s := ParticleSystem(BURST))
			self.add_widget(s)

	def on_reversed(self, _, value):
		self.ids.grid.orientation = "lr-bt" if value else "rl-tb"
		self.ids.bg.orientation = "lr-bt" if value else "rl-tb"

		if self.ids.grid.children:
			for i in range(210):
				if isinstance(n := self.ids.grid.children[i], CroppedImage):
					n.image.pos_hint = self.ids.bg.children[i].image.pos_hint = {'top': 2 - value}

	def on_paused(self, _, paused):
		Animation(opacity = int(not paused), duration = 0.2).start(self.ids.grid)

	def start(self):
		self.ids.grid.opacity = 0
		Animation(opacity = 1, duration = 0.2).start(self.ids.grid)

	def MatrixUpdate(self, matrix: bytearray):
		for n, i in enumerate(matrix):
			if n == 210: break
			self.ids.grid.children[n].texture = TileSet[i]

	def AttackColumnUpdate(self, value: int):
		if self.use_attack_column:
			self.ids.attack_col.set(value)

	def RewardedAction(self, action: str, score: int, b2b: bool = False, time: float = 0.5):
		self.ids.action_text.opacity = 0
		self.ids.action_text.text = action + "\nBack-to-Back" * b2b + f"[size={round(self.ids.action_text.font_size/2)}]\n{score:+}[/size]"
		a = Animation(duration = time / 2, opacity = 1, transition = "out_back") \
			+ Animation(duration = time / 2, opacity = 0, transition = "in_back")
		a.start(self.ids.action_text)

	def LevelUpdate(self, level: int, time: float = 0.5):
		self.ids.lvlup_text.opacity = 0
		self.ids.lvlup_text.text = f"Niveau Supérieur" + f"[size={round(self.ids.lvlup_text.font_size/2)}]\nScore x{level}[/size]\n\n\n\n"
		a = Animation(duration = time / 2, opacity = 1, transition = "out_back") \
			+ Animation(duration = time / 2, opacity = 0, transition = "in_back")
		a.start(self.ids.lvlup_text)

	def End(self, text: str, time: float = 2):
		self.ids.end_text.opacity = 0
		self.ids.end_text.text = text

		a = Animation(duration = time / 2, opacity = 1, transition = "out_back") \
			+ Animation(duration = time / 2, opacity = 0, transition = "in_back")
		a.bind(on_complete = lambda *x: setattr(self.ids.end_text, "text", ""))
		a.start(self.ids.end_text)

	def Countdown(self, callback: Callable = None):
		self.ids.countdown_text.opacity = 0
		self.ids.countdown_text.text = ""

		def oc(_, tx):
			a = Animation(duration = 0.5, opacity = 1, transition = "out_back") \
				+ Animation(duration = 0.5, opacity = 0, transition = "in_back")
			a.bind(on_complete = oc)

			match tx.text:
				case "": tx.text = "3"
				case "3": tx.text = "2"
				case "2": tx.text = "1"
				case "1":
					tx.text = "Partez!"
					tx.font_size /= 2
				case _:
					tx.font_size *= 2
					return callback()

			a.start(tx)

		oc(None, self.ids.countdown_text)

	def TetriminoHardDrop(self, src: tuple, dst: tuple, vs: tuple):
		if self.reversed:
			xd = self.x + self.cell_size[0] * (10 - dst[0] % 10)
			yd = self.y + self.cell_size[1] * (20.5 - dst[1])
		else:
			xd = self.x + self.cell_size[0] * (dst[0] % 10)
			yd = self.y + self.cell_size[1] * dst[1]

		self.hd_particle.pos = (xd, yd)

		self.hd_particle.emitter_x_variance = self.cell_size[0] * vs[0] / 2
		self.hd_particle.emitter_y_variance = self.cell_size[1] * vs[1] / 2

		self.hd_particle.start(0.2)

	def MatrixLineClear(self, lineclear: tuple[int, ...], time: float = 0.3):
		a = Animation(duration = time, x = self.x + self.cell_size[0] * 9.5)

		for i, line in enumerate(lineclear):
			if not 0 <= line <= 21: continue

			def x(s: ParticleSystem, d: float):
				y = self.y + self.cell_size[1] * ((20.5 - line) if self.reversed else (line + 1))
				if d != y: s.y = y

			s = self.particles[line]
			s.pos = self.x + self.cell_size[0] / 2, self.y + self.cell_size[1] * ((20.5 - line) if self.reversed else (line + 1))
			s.emitter_x_variance, s.emitter_y_variance = self.cell_size[0] / 4, self.cell_size[1] / 4
			s.bind(y = x)
			s.start(time)
			a.start(s)

	def LockDownStartAnim(self, timeout: float, c: set):
		for x in self.c:
			self.ids.grid.children[x].opacity = 1

		self.c.clear()
		self.lockdown_anim._duration = timeout
		for x in filter((210).__gt__, c):
			n = self.ids.grid.children[x]
			n.opacity = timeout * 1.3 + 0.35
			self.lockdown_anim.start(n)
			self.c.append(x)

	def LockDownMoveAnim(self, timeout: float, c: set):
		if not self.c: return self.LockDownStartAnim(c)

		frm = self.ids.grid.children[self.c[0]].opacity

		for x in self.c:
			self.lockdown_anim.stop(n := self.ids.grid.children[x])
			if x not in c: n.opacity = 1

		self.c.clear()
		self.lockdown_anim._duration = timeout
		for x in filter((210).__gt__, c):
			(n := self.ids.grid.children[x]).opacity = frm
			self.lockdown_anim.start(n)
			self.c.append(x)

	def LockDownStopAnim(self):
		for x in self.c:
			self.lockdown_anim.stop(n := self.ids.grid.children[x])
			n.opacity = 1
		self.c.clear()

	def LockDownPauseAnim(self):
		for x in self.c:
			self.lockdown_anim.stop(self.ids.grid.children[x])

	def LockDownResumeAnim(self, timeout: float):
		self.lockdown_anim._duration = timeout
		for x in self.c:
			self.lockdown_anim.start(self.ids.grid.children[x])
