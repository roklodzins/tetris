from kivy.lang import Builder
from os.path import join, dirname
from kivy import require


require("2.1.0")


Builder.load_file(join(dirname(__file__), "style.kv"), rulesonly = True)


from core.gfx.menu import (TitleScreen, MainMenuScreen, GameConfigScreen,
                           ResultScreen, ControllerScreen, SettingsScreen,
                           HelpScreen, HelpMenu)
from core.gfx.misc import (AdvScreen, AdvScreenManager, TableTile, Table,
                           AdvLabel, get_text_size, Spinbox, SpinboxOption,
                           AdvCheckbox, AdvButton)
from core.gfx.gamefield import GameFieldScreen, GameField, GameFieldMulti, GameFieldMultiScreen


__all__ = ("TitleScreen", "MainMenuScreen", "GameConfigScreen",
           "ResultScreen", "ControllerScreen", "SettingsScreen",
           "HelpScreen", "HelpMenu",
           "AdvScreen", "AdvScreenManager", "TableTile", "Table",
           "AdvLabel", "get_text_size", "Spinbox", "SpinboxOption",
           "AdvCheckbox", "AdvButton",
           "GameFieldScreen", "GameField", "GameFieldMulti", "GameFieldMultiScreen")
