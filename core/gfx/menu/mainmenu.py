from core.controller import Input, InputManager, WiimoteEvent, XinputEvent
from core.games import Game
from core.gfx.misc import AdvScreen
from core.misc import TileSetAccess, PlayerSave

from kivy.animation import Animation
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.behaviors import HoverBehavior


__all__ = "MainMenuScreen",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class MainMenuScreen(AdvScreen):
	name = StringProperty("main_menu_screen")
	input = ObjectProperty(None, allownone = True)

	def on_kv_post(self, base_widget):
		for i in Game:
			self.ids.swiper.add_widget(GameTile(game = i, manager = self.manager))
		self.ids.swiper.add_widget(SettingsTile(manager = self.manager))

	def on_pre_enter(self):
		super().on_pre_enter()

	def prev(self, *_):
		self.ids.swiper.prev()

	def next(self, *_):
		self.ids.swiper.next()

	def on_enter(self):
		super().on_enter()
		self.input = InputManager(Input("spacebar", "enter", XinputEvent.A, XinputEvent.B,
		                                XinputEvent.BACK, XinputEvent.START,
		                                WiimoteEvent.WII_A, WiimoteEvent.WII_1,
		                                WiimoteEvent.WII_2,
		                                action_down = lambda *x: self.ids.swiper.get_current_item().on_release()),
		                          l := Input("left", PlayerSave.keyboard_map["move_left"],
		                                     XinputEvent.LEFT_SHOULDER, XinputEvent.LSTICK_LEFT, XinputEvent.DPAD_LEFT,
		                                     WiimoteEvent.WII_UP, WiimoteEvent.WII_MINUS,
		                                     action_down = self.prev, repeat_after = 0.25,
		                                     repeat_interval = 0.25),
		                          r := Input("right", PlayerSave.keyboard_map["move_right"],
		                                     XinputEvent.RIGHT_SHOULDER, XinputEvent.LSTICK_RIGHT, XinputEvent.DPAD_RIGHT,
		                                     WiimoteEvent.WII_DOWN, WiimoteEvent.WII_PLUS,
		                                     action_down = self.next, repeat_after = 0.25,
		                                     repeat_interval = 0.25)
		                          )
		l.set_opposite(r)

	def on_pre_leave(self):
		super().on_pre_leave()
		self.input.clear()
		self.input = None


class HoverButtonBehavior(HoverBehavior):
	shadow_animation = ObjectProperty()

	def on_enter(self, *args):
		if self.shadow_animation:
			Animation.cancel_all(self, "opacity")
		self.shadow_animation = Animation(opacity = 1.15, d = 0.4)
		self.shadow_animation.start(self)

	def on_leave(self, *args):
		if self.shadow_animation:
			Animation.cancel_all(self, "opacity")
		self.shadow_animation = Animation(opacity = 1, d = 0.2)
		self.shadow_animation.start(self)


class BaseTile(ButtonBehavior, HoverButtonBehavior, TileSetAccess, BoxLayout):
	manager = ObjectProperty(None)
	title = StringProperty("")
	desc = StringProperty("")
	subtext = StringProperty("")


class GameTile(BaseTile):
	game = ObjectProperty(None)

	def on_release(self):
		self.manager.game = self.game
		self.manager.current = "gameconfig_screen"


class SettingsTile(BaseTile):
	def on_release(self):
		self.manager.current = "settings_screen"
