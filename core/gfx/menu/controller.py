from core.controller import (XInputControllerManager, XinputEvent,
							 WiimoteControllerManager, WiimoteEvent,
							 InputManager, Input)

from core.gfx.gamefield import GameFieldMultiScreen
from core.gfx.misc import AdvScreen

from kivy.lang import Builder
from kivy.properties import BooleanProperty, ObjectProperty, StringProperty


__all__ = "ControllerScreen",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class ControllerScreen(AdvScreen):
	name = StringProperty("controller_screen")
	input = ObjectProperty(None, allownone = True)
	table = ObjectProperty(None, allownone = True)
	states = ObjectProperty(None, allownone = True)
	can_start = BooleanProperty(False)

	def controller_connected(self, controller: str | None):
		if not controller: controller = "KEYBOARD"
		if controller not in self.states:
			self.states[controller] = min(range(self.game.max_player_num),
										  key = lambda x: list(self.states.values()).count(x))
			self.update_table()

	def controller_disconnected(self, controller: str | None):
		if controller in self.states:
			del self.states[controller]
			self.update_table()

	def controller_left(self, controller: str | None):
		if not controller: controller = "KEYBOARD"
		if controller in self.states and self.states[controller] > 0:
			self.states[controller] -= 1
			self.update_table()

	def controller_right(self, controller: str | None):
		if not controller: controller = "KEYBOARD"
		if controller in self.states and self.states[controller] < self.game.max_player_num - 1:
			self.states[controller] += 1
			self.update_table()

	def on_enter(self):
		super().on_enter()
		self.input = InputManager(c := Input(XinputEvent.CONNECT_STATE, WiimoteEvent.CONNECT_STATE,
		                                     action_down = self.controller_connected,
		                                     action_up = self.controller_disconnected),
		                          Input(XinputEvent.A, XinputEvent.START, XinputEvent.BACK, WiimoteEvent.WII_A, WiimoteEvent.WII_2,
		                                "spacebar", "enter",
		                                action_down = self.start),
								  l := Input(XinputEvent.LEFT_SHOULDER, XinputEvent.LSTICK_LEFT, XinputEvent.DPAD_LEFT,
								             WiimoteEvent.WII_UP, WiimoteEvent.WII_MINUS,
											 action_down = self.controller_left, repeat_after = 0.4, repeat_interval = 0.2),
								  r := Input(XinputEvent.RIGHT_SHOULDER, XinputEvent.LSTICK_RIGHT, XinputEvent.DPAD_RIGHT,
								             WiimoteEvent.WII_DOWN, WiimoteEvent.WII_PLUS,
											 action_down = self.controller_right, repeat_after = 0.4, repeat_interval = 0.2)
								  )
		l.set_opposite(r)

		self.states = {}

		self.table = [[" "] + [f"J{i + 1}" for i in range(self.game.max_player_num)]]

		for i in range(9):
			co = True
			if i == 0:
				self.table.append(["Clavier"])
				self.states["KEYBOARD"] = 0
			elif i < 5:
				self.table.append([f"Xbox {i}"])
				xi = XInputControllerManager.get(i - 1)
				if xi.connected:
					self.states[xi.idx] = min(range(self.game.max_player_num),
					                          key = lambda x: list(self.states.values()).count(x))
			else:
				self.table.append([f"Wiimote {i - 4}"])
				wm = WiimoteControllerManager.get(i - 5)
				if wm.connected:
					self.states[wm.idx] = min(range(self.game.max_player_num),
					                          key = lambda x: list(self.states.values()).count(x))
			self.table[-1].extend("Ok" if (u == i or i >= self.game.max_player_num and not u) and co else "" for u in range(self.game.max_player_num))

		self.update_table()

		for i in XInputControllerManager.get_connected():
			InputManager.dispatch_keydown(XinputEvent.CONNECT_STATE, i.idx)
		for i in WiimoteControllerManager.get_connected():
			InputManager.dispatch_keydown(WiimoteEvent.CONNECT_STATE, i.idx)


	def update_table(self):
		self.can_start = len(set(self.states.values())) > 1

		for i in range(9):
			self.table[1 + i][1:] = [""] * self.game.max_player_num
			if i == 0:
				self.table[1][1 + self.states["KEYBOARD"]] = "Ok"
			elif i < 5:
				if (n := self.states.get(XInputControllerManager.get(i - 1).idx, None)) is not None:
					self.table[1 + i][1 + n] = "Ok"
			else:
				if (n := self.states.get(WiimoteControllerManager.get(i - 5).idx, None)) is not None:
					self.table[1 + i][1 + n] = "Ok"
		self.ids.table.set(self.table)

	def cancel(self):
		self.manager.current = "gameconfig_screen"

	def start(self, *_):
		ctr = {}
		for k, p in self.states.items():
			if p not in ctr: ctr[p] = []
			ctr[p].append(k)

		if self.can_start:
			self.manager.add_widget(GameFieldMultiScreen(ctr))
			self.manager.current = "gamefield_screen"

	def on_pre_leave(self):
		super().on_pre_leave()
		self.input.clear()
		self.input = None
		self.states = None
