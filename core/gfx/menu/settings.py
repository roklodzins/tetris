from core.gfx.misc import AdvScreen, get_text_size
from core.misc import PlayerSave, TileSetAccess, LockdownMode

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import NumericProperty, ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.textfield import MDTextField


__all__ = "SettingsScreen",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class AdvTextField(MDTextField):

	def set_objects_labels(self):  # repair KivyMD
		super().set_objects_labels()
		self._hint_text_label.font_name = "GameFont"
		self._max_length_label.font_name = "GameFont"
		self._helper_text_label.font_name = "GameFont"


class TextFieldBox(BoxLayout, TileSetAccess):

	def on_children(self, _, c):
		if c: self.padding = c[0].font_size, c[0].font_size / 2, c[0].font_size, c[0].font_size / 2


class Keymap(AdvTextField):
	def keyboard_on_key_down(self, window, keycode, text, modifiers):  # work around KivyMD
		Clock.schedule_once(lambda x: setattr(self, 'text', f"<{keycode[1]}>" if keycode[1] else ""), -1)

	def next(self, *_):
		if not self.error:
			x = self.parent.parent
			try: x.children[x.children.index(self.parent) - 1].children[0].focus = True
			except: pass

	def on_text(self, _, text):
		self.error = not text
		Clock.schedule_once(self.next, -1)



class SettingsScreen(AdvScreen, TileSetAccess):
	save: PlayerSave = ObjectProperty(PlayerSave)
	name = StringProperty("settings_screen")
	section_size = NumericProperty(20)
	text_size = NumericProperty(10)
	errors = ObjectProperty()

	def __init__(self, **k):
		super().__init__(**k)
		self.errors = set()

	def _check_radio(self, inst):
		if not self.ids.cls_pl.active and \
				not self.ids.ext_pl.active and \
				not self.ids.inf_pl.active: inst.active = True

	def check_radio(self, inst):  # repair KivyMD: always 1 radio active
		Clock.schedule_once(lambda *x: self._check_radio(inst), -1)


	def check_key(self, _):
		f = set()
		for i in self.ids.keymap.children:
			for u in self.ids.keymap.children:
				if i is u: break
				k = i.children[0]
				o = u.children[0]
				if k.text == o.text != "":
					f.add(k)
					f.add(o)

		for i in self.ids.keymap.children:
			k.error = not ((k := i.children[0]) in f or not k.text)
			k.error = not k.error  # repair KivyMD

	def check(self, inst):
		if inst.error: self.errors.add(id(inst))
		elif id(inst) in self.errors: self.errors.remove(id(inst))
		self.ids.exit.disabled = bool(self.errors)

	def check_name(self):
		self.ids.player_name.error = get_text_size(self.ids.player_name.text, 1000) > 9690

	def on_pre_enter(self):
		super().on_pre_enter()

	def apply(self):
		self.save.player_name = self.ids.player_name.text
		self.save.theme = self.ids.theme.value
		if self.tileset.theme != self.save.theme: self.tileset.theme = self.save.theme
		self.save.auto_repeat_delay = self.ids.auto_repeat_delay.value / 1000
		self.save.auto_repeat_trigger = self.ids.auto_repeat_trigger.value / 1000
		self.save.is_ghost_allowed = self.ids.is_ghost_allowed.active
		self.save.is_hold_allowed = self.ids.is_hold_allowed.active

		match True:
			case self.ids.cls_pl.active: self.save.lockdown_mode = LockdownMode.Classic
			case self.ids.inf_pl.active: self.save.lockdown_mode = LockdownMode.InfinitePlacement
			case _: self.save.lockdown_mode = LockdownMode.ExtendedPlacement

		self.save.keyboard_map = {
			"pause": self.ids.pause.text[1:-1],
			"hold": self.ids.hold.text[1:-1],
			"move_left": self.ids.move_left.text[1:-1],
			"move_right": self.ids.move_right.text[1:-1],
			"soft_drop": self.ids.soft_drop.text[1:-1],
			"hard_drop": self.ids.hard_drop.text[1:-1],
			"rotate_clock": self.ids.rotate_clock.text[1:-1],
			"rotate_aclock": self.ids.rotate_aclock.text[1:-1]
		}

		self.save.save()

		self.manager.current = "main_menu_screen"

	def help(self):
		self.manager.current = "help_screen"
