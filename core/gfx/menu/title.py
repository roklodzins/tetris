from core.controller import Input, InputManager
from core.gfx.misc import AdvScreen
from core.misc import PlayerSave

from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.behaviors import ButtonBehavior


__all__ = "TitleScreen",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class TitleScreen(ButtonBehavior, AdvScreen):
	name = StringProperty("title_screen")
	input = ObjectProperty(None, allownone = True)

	def __init__(self, **k):
		super().__init__(**k)

	def on_enter(self):
		self.input = InputManager(Input(action_up = self.to_menu))

	def to_menu(self, *_):
		self.input.clear()
		self.input = None
		self.manager.current = "main_menu_screen" if PlayerSave.loaded else "help_screen"
