import sys, kivy, kivymd

from core.gfx.misc import AdvScreen
from core.misc import TileSet

from kivy.lang import Builder
from kivy.properties import NumericProperty, ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout


__all__ = "HelpScreen", "HelpMenu"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class HelpScreen(AdvScreen):
	name = StringProperty("help_screen")

	def quit(self):
		self.manager.current = "settings_screen"

	def on_pre_enter(self):
		super().on_pre_enter()
		self.ids.help.reset()


class HelpMenu(BoxLayout):
	tileset = ObjectProperty(TileSet)
	current = NumericProperty(-1)
	quit = ObjectProperty(lambda: None)

	PyVer = StringProperty(".".join(map(str, sys.version_info[:3])))
	KivyVer = StringProperty(kivy.__version__)
	KivymdVer = StringProperty(kivymd.__version__)

	def on_kv_post(self, base_widget):
		for n, i in enumerate(self.ids.buttons.children):
			i.bind(on_release = lambda *x, n = n: setattr(self, "current", n))

	def reset(self):
		self.current = self.ids.buttons.children.index(self.ids.default)

	def on_current(self, _, c):
		for n, i in enumerate(self.ids.help.children):
			i.opacity = int(r := (n == c))
			if r: self.ids.subtitle.text = self.ids.buttons.children[n].text
