from core.controller import Input, InputManager, WiimoteEvent, XinputEvent
from core.gfx.misc import AdvScreen
from core.misc import EndCondition, PlayerSave

from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty


__all__ = "ResultScreen",


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class ResultScreen(AdvScreen):
	name = StringProperty("result_screen")
	engine = ObjectProperty(None)
	input = ObjectProperty(None, allownone = True)
	title = StringProperty("")

	def set_result(self, engine):
		game = self.game
		self.engine = engine

		self.title = (game.name + " - " if game else "") + "Victoire" if engine.game_over != EndCondition.GameOver else "Défaite" if engine else "Résultat"

		result = {i.key: i.getter(engine) for i in game.rank_result}

		data = [["Résultat"] + [i.name for i in game.rank_result], *PlayerSave.get_results(game.name, result, game.rank_result)]

		for n, i in enumerate(game.rank_result):
			for d in data[1:]: d[n + 1] = i.form(d[n + 1])


		for i in range(10 - len(data)):
			data.append([''] * len(data[0]))
		self.ids.table.set(data)

	def set_multi_result(self, server):
		data = [["Résultat"] + [i.name for i in server.rank_result]]
		d = [[f"Joueur {num + 1}"] + [i.getter(e) for i in server.rank_result] for num, e in server.engines.items()]

		self.title = server.name

		for n, i in enumerate(reversed(server.rank_result)):
			d.sort(key = lambda x: i.sort_key(x[len(server.rank_result) - n]), reverse = True)
			for x in d: x[len(server.rank_result) - n] = i.form(x[len(server.rank_result) - n])

		data.extend(d)

		for i in range(10 - len(data)):
			data.append([''] * len(data[0]))
		self.ids.table.set(data)


	def quit(self):
		self.manager.current = "main_menu_screen"

	def start(self):
		self.manager.current = "gameconfig_screen"

	def on_enter(self):
		super().on_enter()

		self.input = InputManager(Input("escape", "backspace", XinputEvent.BACK, XinputEvent.B,
		                                WiimoteEvent.WII_B, WiimoteEvent.WII_1,
		                                action_down = lambda *x: self.quit()),
		                          Input("enter", "spacebar",
		                                XinputEvent.START, XinputEvent.A,
		                                WiimoteEvent.WII_A, WiimoteEvent.WII_2,
		                                action_down = lambda *x: self.start())
		                          )

	def on_pre_leave(self):
		super().on_pre_leave()
		self.input.clear()
		self.input = None
