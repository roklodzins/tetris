from core.controller import InputManager, Input, XinputEvent, WiimoteEvent
from core.gfx.gamefield import GameFieldScreen
from core.gfx.misc import AdvScreen, AdvCheckbox, Spinbox

from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout


__all__ = "GameConfigScreen"


Builder.load_file(__file__[:-2] + "kv", rulesonly = True)


class GameConfigScreen(AdvScreen):
	name = StringProperty("gameconfig_screen")
	input = ObjectProperty(None, allownone = True)

	def on_pre_enter(self):
		super().on_pre_enter()

		self.ids.title.text = self.game.name

		field: BoxLayout = self.ids.field
		for i in list(field.children): field.remove_widget(i)

		self.params = {}

		if self.game.allow_config_cycle_matrix or \
			self.game.allow_config_variable_goal or \
			self.game.allow_config_random_tetrimino or \
			self.game.allow_config_reversed_matrix or \
			self.game.allow_config_starting_level:

			if self.game.allow_config_cycle_matrix:
				self.params["use_cycle_matrix"] = n = AdvCheckbox(active = self.game.use_cycle_matrix,
				                                                  text = "Matrice cyclique - Retire les limites latérales")
				field.add_widget(n)

			if self.game.allow_config_reversed_matrix:
				self.params["use_reversed_matrix"] = n = AdvCheckbox(active = self.game.use_reversed_matrix,
				                                                     text = "Matrice renversée - Inverse la gravité")
				field.add_widget(n)

			if self.game.allow_config_random_tetrimino:
				self.params["use_random_tetrimino"] = n = AdvCheckbox(active = self.game.use_random_tetrimino,
				                                                      text = "Multiminos aléatoires - Remplace les Tetriminos par des Multiminos")
				field.add_widget(n)

			if self.game.allow_config_variable_goal:
				self.params["use_variable_goal"] = n = AdvCheckbox(active = self.game.use_variable_goal,
				                                                   text = "Objectif Variable - Change le système de score et de niveau")
				field.add_widget(n)

			if self.game.allow_config_starting_level:
				self.params["starting_level"] = n = Spinbox(value = self.game.starting_level,
				                                            min = 1,
				                                            max = 15,
				                                            increment = 1,
				                                            format = "Niveau {}".format)
				field.add_widget(n)

	def cancel(self):
		self.manager.current = "main_menu_screen"

	def start(self):
		self.game = self.game()
		for i, u in self.params.items():
			setattr(self.game, i, u.get())

		if self.game.is_multiplayer:
			self.manager.current = "controller_screen"
		else:
			self.manager.add_widget(GameFieldScreen())
			self.manager.current = "gamefield_screen"

	def on_enter(self):
		super().on_enter()
		self.input = InputManager(Input("escape", "backspace", XinputEvent.BACK, XinputEvent.B,
		                                WiimoteEvent.WII_B, WiimoteEvent.WII_1,
		                                action_down = lambda *x: self.cancel()),
		                          Input("enter", "spacebar",
		                                XinputEvent.START, XinputEvent.A,
		                                WiimoteEvent.WII_A, WiimoteEvent.WII_2,
		                                action_down = lambda *x: self.start())
		                          )

	def on_pre_leave(self):
		super().on_pre_leave()
		self.input.clear()
		self.input = None
