from random import random

from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.graphics import (Color, Callback, Rotate, PushMatrix, PopMatrix, Translate, Scale, Point)
from kivy.graphics.opengl import (glBlendFunc, GL_SRC_ALPHA, GL_ONE,
								  GL_ZERO, GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR, GL_ONE_MINUS_SRC_ALPHA,
								  GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA, GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR)

from kivy.properties import (NumericProperty, BooleanProperty, ListProperty, StringProperty, ObjectProperty)
from kivy.vector import Vector

from math import radians, degrees, cos, sin

import warnings

warnings.simplefilter("once", Warning)
warnings.warn("Compiled particle system unavailable: performance can be reduced", Warning)

EMITTER_TYPE_GRAVITY = 0
EMITTER_TYPE_RADIAL = 1

BLEND_FUNC = {
	0    : GL_ZERO,
	1    : GL_ONE,
	0x300: GL_SRC_COLOR,
	0x301: GL_ONE_MINUS_SRC_COLOR,
	0x302: GL_SRC_ALPHA,
	0x303: GL_ONE_MINUS_SRC_ALPHA,
	0x304: GL_DST_ALPHA,
	0x305: GL_ONE_MINUS_DST_ALPHA,
	0x306: GL_DST_COLOR,
	0x307: GL_ONE_MINUS_DST_COLOR}


def random_variance(base: float, variance: float) -> float:
	return base + variance * (random() * 2.0 - 1.0)


def random_color_variance(base: list, variance: list) -> list:
	return [min(max(0.0, b + v * (random() * 2.0 - 1.0)), 1.0) for b, v in zip(base, variance)]


class Particle:
	__slots__ = ("x", "y", "rotation", "current_time", "scale", "total_time", "start_x", "start_y",
				 "velocity_x", "velocity_y", "radial_acceleration", "tangent_acceleration",
				 "emit_radius", "emit_radius_delta", "emit_rotation", "emit_rotation_delta",
				 "rotation_delta", "scale_delta", "color", "color_delta")

	def __init__(self):
		self.x, self.y, self.rotation, self.current_time = -256, -256, 0, 0
		self.scale, self.total_time = 1.0, 0.
		self.start_x = self.start_y = self.velocity_x = self.velocity_y = self.radial_acceleration = \
			self.tangent_acceleration = self.emit_radius = self.emit_radius_delta = self.emit_rotation = \
			self.emit_rotation_delta = self.rotation_delta = self.scale_delta = 0

		self.color = [1.0, 1.0, 1.0, 1.0]
		self.color_delta = [0.0, 0.0, 0.0, 0.0]


class ParticleSystem(Widget):
	max_num_particles = NumericProperty(200)
	life_span = NumericProperty(2)

	texture = ObjectProperty(None)
	texture_path = StringProperty(None)

	life_span_variance = NumericProperty(0)

	start_size = NumericProperty(16)
	start_size_variance = NumericProperty(0)

	end_size = NumericProperty(16)
	end_size_variance = NumericProperty(0)

	emit_angle = NumericProperty(0)
	emit_angle_variance = NumericProperty(0)

	sync_emit_angle_rotation = BooleanProperty(False)

	start_rotation = NumericProperty(0)
	start_rotation_variance = NumericProperty(0)

	end_rotation = NumericProperty(0)
	end_rotation_variance = NumericProperty(0)

	emitter_x_variance = NumericProperty(100)
	emitter_y_variance = NumericProperty(100)

	gravity_x = NumericProperty(0)
	gravity_y = NumericProperty(0)

	speed = NumericProperty(0)
	speed_variance = NumericProperty(0)

	radial_acceleration = NumericProperty(100)
	radial_acceleration_variance = NumericProperty(0)

	tangential_acceleration = NumericProperty(0)
	tangential_acceleration_variance = NumericProperty(0)

	max_radius = NumericProperty(100)
	max_radius_variance = NumericProperty(0)

	min_radius = NumericProperty(50)

	rotate_per_second = NumericProperty(0)
	rotate_per_second_variance = NumericProperty(0)

	start_color = ListProperty([1., 1., 1., 1.])
	start_color_variance = ListProperty([1., 1., 1., 1.])

	end_color = ListProperty([1., 1., 1., 1.])
	end_color_variance = ListProperty([1., 1., 1., 1.])

	blend_factor_source = NumericProperty(GL_SRC_ALPHA)
	blend_factor_dest = NumericProperty(GL_ONE)

	reset_blend_factor_source = NumericProperty(GL_SRC_ALPHA)
	reset_blend_factor_dest = NumericProperty(GL_ONE_MINUS_SRC_ALPHA)

	emitter_type = NumericProperty(0)

	current_scroll = ListProperty((0, 0))
	update_interval = NumericProperty(1. / 30.)
	friction = NumericProperty(0.0)
	_is_paused = BooleanProperty(False)
	_event = ObjectProperty(None)

	def __init__(self, config = None, **kwargs):
		self.particles = list()

		self.capacity = 0
		self.particles_dict = dict()
		self.emission_time = 0.0
		self.frame_time = 0.0
		self.num_particles = 0

		if config is not None:
			self._parse_config(config)

		self.emission_rate = self.max_num_particles / self.life_span
		self.max_capacity = self.max_num_particles
		self._event = Clock.schedule_interval(self._update, self.update_interval)
		self._event.cancel()

		super().__init__(**kwargs)

		with self.canvas.before:
			Callback(self._set_blend_func)
		with self.canvas.after:
			Callback(self._reset_blend_func)

	def on_update_interval(self, _, i):
		self._event.timeout = i

	def start(self, duration = 0x7fffffffffffffff):
		if self.emission_rate != 0:
			self.emission_time = duration
			self._event()

	def stop(self, clear = False):
		self.emission_time = 0.0
		if clear:
			self.num_particles = 0
			self.particles_dict = dict()
			self.particles = []
			self.capacity = 0
			self.canvas.clear()
			self._event.cancel()

	def on_max_num_particles(self, _, value):
		self.max_capacity = value
		if self.capacity > value:
			self._lower_capacity(self.capacity - self.max_capacity)
		self.emission_rate = self.max_num_particles / self.life_span

	def on_texture(self, *_):
		for particle in self.particles:
			try:
				self.particles_dict[particle]['rect'].texture = self.texture
			except KeyError:
				# if particle isn't initialized yet, you can't change
				# its texture.
				pass

	def on_life_span(self, _, value):
		self.emission_rate = self.max_num_particles / value

	def _set_blend_func(self, _):
		glBlendFunc(self.blend_factor_source, self.blend_factor_dest)

	def _reset_blend_func(self, _):
		glBlendFunc(self.reset_blend_factor_source, self.reset_blend_factor_dest)

	def _parse_config(self, config: dict):
		config = config.copy()

		if 'life_span' in config:
			config['life_span'] = max(0.01, config['life_span'])

		if 'emitter_type' in config:
			config['emitter_type'] = int(config['emitter_type'])

		for i in filter(config.__contains__, ('emit_angle', 'emit_angle_variance', 'start_rotation', 'start_rotation_variance',
											  'end_rotation', 'end_rotation_variance', 'rotate_per_second',
											  'rotate_per_second_variance')):
			config[i] = radians(config[i])

		for i in ('blend_factor_source', 'blend_factor_dest'):
			if i in config:
				config[i] = BLEND_FUNC[config[i]]

		for i, u in config.items():
			if hasattr(self, i):
				setattr(self, i, u)
			else: raise NameError(i)

	def pause(self, with_clear = False):
		self._is_paused = True
		if with_clear:
			particles = self.particles
			for particle in particles:
				particle.current_time = particle.total_time
			self._event.cancel()

	def resume(self):
		self._is_paused = False
		self._event()

	def _update(self, dt: float):
		self._advance_time(dt)
		self._render()
		if not self.emission_time:
			if all(x['translate'].x == 4096 for x in self.particles_dict.values()):
				return False
		return not self._is_paused

	@staticmethod
	def _create_particle():
		return Particle()

	def _init_particle(self, particle: Particle):
		life_span: float = random_variance(self.life_span, self.life_span_variance)
		if life_span <= 0.0: return

		particle.current_time = 0.0
		particle.total_time = life_span
		particle.x = random_variance(self.pos[0], self.emitter_x_variance)
		particle.y = random_variance(self.pos[1], self.emitter_y_variance)
		particle.start_x, particle.start_y = self.pos

		angle: float = random_variance(self.emit_angle, self.emit_angle_variance)
		speed: float = random_variance(self.speed, self.speed_variance)
		particle.velocity_x = speed * cos(angle)
		particle.velocity_y = speed * sin(angle)

		particle.emit_radius = random_variance(self.max_radius,
											   self.max_radius_variance)
		particle.emit_radius_delta = (self.max_radius -
									  self.min_radius) / life_span

		particle.emit_rotation = random_variance(self.emit_angle,
												 self.emit_angle_variance)
		particle.emit_rotation_delta = random_variance(self.rotate_per_second,
													   self.rotate_per_second_variance)

		particle.radial_acceleration = random_variance(
				self.radial_acceleration,
				self.radial_acceleration_variance)
		particle.tangent_acceleration = random_variance(
				self.tangential_acceleration,
				self.tangential_acceleration_variance)

		start_size: float = random_variance(self.start_size, self.start_size_variance)
		end_size: float = random_variance(self.end_size, self.end_size_variance)

		start_size = max(0.1, start_size)
		end_size = max(0.1, end_size)

		particle.scale = start_size / 2.
		particle.scale_delta = ((end_size - start_size) / life_span) / 2.  # / cls.texture.width

		# colors
		start_color: list = random_color_variance(self.start_color, self.start_color_variance)
		end_color: list = random_color_variance(self.end_color, self.end_color_variance)

		particle.color_delta = [(end_color[i] - start_color[i]) / life_span for i in range(4)]
		particle.color = start_color

		# rotation
		if self.sync_emit_angle_rotation:
			particle.rotation = angle
			particle.rotation_delta = 0
		else:
			particle.rotation = random_variance(self.start_rotation, self.start_rotation_variance)
			particle.rotation_delta = (random_variance(self.end_rotation,
													   self.end_rotation_variance) - particle.rotation) / life_span

	def _advance_particle_gravity(self, particle: Particle, passed_time: float):
		distance_x: float = particle.x - particle.start_x
		distance_y: float = particle.y - particle.start_y
		start_pos: tuple = particle.start_x, particle.start_y
		current_pos: tuple = particle.x, particle.y
		distance_scalar: float = Vector(current_pos).distance(start_pos)
		if distance_scalar < 0.01: distance_scalar = 0.01

		radial_x: float = distance_x / distance_scalar
		radial_y: float = distance_y / distance_scalar
		tangential_x: float = radial_x
		tangential_y: float = radial_y

		radial_x *= particle.radial_acceleration
		radial_y *= particle.radial_acceleration

		new_y: float = tangential_x
		tangential_x = -tangential_y * particle.tangent_acceleration
		tangential_y = new_y * particle.tangent_acceleration

		particle.velocity_x += passed_time * (self.gravity_x +
											  radial_x + tangential_x)
		particle.velocity_y += passed_time * (self.gravity_y +
											  radial_y + tangential_y)

		particle.velocity_x -= particle.velocity_x * self.friction
		particle.velocity_y -= particle.velocity_y * self.friction

		particle.x += particle.velocity_x * passed_time
		particle.y += particle.velocity_y * passed_time

	def _advance_particle(self, particle: Particle, passed_time: float):
		passed_time = min(passed_time, particle.total_time - particle.current_time)
		particle.current_time += passed_time

		if self.emitter_type == EMITTER_TYPE_RADIAL:
			particle.emit_rotation += (particle.emit_rotation_delta * passed_time)
			particle.emit_radius -= particle.emit_radius_delta * passed_time
			particle.x = (self.pos[0] - cos(particle.emit_rotation) * particle.emit_radius)
			particle.y = (self.pos[1] - sin(particle.emit_rotation) * particle.emit_radius)

			if particle.emit_radius < self.min_radius: particle.current_time = particle.total_time

		else: self._advance_particle_gravity(particle, passed_time)

		particle.scale += particle.scale_delta * passed_time
		particle.rotation += particle.rotation_delta * passed_time

		particle.color = [particle.color[i] + particle.color_delta[i] * passed_time for i in range(4)]

	def _lower_capacity(self, by_amount):
		old_capacity = self.capacity
		new_capacity = max(0, self.capacity - by_amount)

		for i in range(int(old_capacity - new_capacity)):
			try: self.remove_particle()
			except: pass

		self.num_particles = int(new_capacity)
		self.capacity = new_capacity

	def remove_particle(self):
		particle_to_remove = self.particles.pop()
		self.canvas.remove(self.particles_dict[particle_to_remove]['rect'])
		del self.particles_dict[particle_to_remove]

	def _advance_time(self, passed_time: float):
		particle_index = 0

		# advance existing particles
		while particle_index < self.num_particles:
			particle = self.particles[particle_index]
			if particle.current_time < particle.total_time:
				self._advance_particle(particle, passed_time)
				particle_index += 1
			else:
				try: self.particles_dict[particle]['translate'].xy = (4096, 4096)
				except: pass
				if particle_index != self.num_particles - 1:
					next_particle = self.particles[self.num_particles - 1]
					self.particles[self.num_particles - 1] = particle
					self.particles[particle_index] = next_particle

				self.num_particles -= 1
		# reinit new particles
		if self.emission_time > 0:
			time_between_particles = 1.0 / self.emission_rate
			self.frame_time += passed_time

			while self.frame_time > 0:
				if self.capacity < self.max_capacity:
					self.particles.append(self._create_particle())
					self.capacity += 1
				if self.num_particles < self.max_capacity:
					particle = self.particles[self.num_particles]
					self.num_particles += 1
					self._init_particle(particle)
					self._advance_particle(particle, self.frame_time)

				self.frame_time -= time_between_particles

			if self.emission_time != 0x7fffffffffffffff:
				self.emission_time = max(0.0, self.emission_time - passed_time)

	def _render(self):
		if self.num_particles == 0:
			return
		particles_dict = self.particles_dict
		particles = self.particles
		texture = self.texture
		current_scroll = self.current_scroll
		for i in range(self.num_particles):
			particle = particles[i]
			if particle not in particles_dict:
				particles_dict[particle] = current_particle = {}
				with self.canvas:
					PushMatrix()
					current_particle['color'] = Color(*particle.color)
					current_particle['translate'] = Translate()
					current_particle['scale'] = Scale(x = particle.scale, y = particle.scale)
					current_particle['rotate'] = Rotate()
					current_particle['rotate'].set(degrees(particle.rotation), 0, 0, 1)
					current_particle['rect'] = Point(texture = texture, points = (0, 0))
					current_particle['translate'].xy = (particle.x + current_scroll[0], particle.y + current_scroll[1])
					PopMatrix()
			else:
				current_particle = particles_dict[particle]
				current_particle['rotate'].angle = degrees(particle.rotation)
				current_particle['scale'].x = particle.scale
				current_particle['scale'].y = particle.scale
				current_particle['translate'].xy = (particle.x + current_scroll[0], particle.y + current_scroll[1])
				current_particle['color'].rgba = particle.color

	def on_current_scroll(self, _, value):
		if not self.num_particles: return
		particles_dict = self.particles_dict
		particles = self.particles
		current_scroll = value
		for index in range(self.num_particles):
			particle = particles[index]
			particles_dict[particle]['translate'].xy = (particle.x +
														current_scroll[0],
														particle.y + current_scroll[1])
