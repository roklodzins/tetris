import pyximport, enum
from kivy.core.image import Image
from os.path import join, dirname

pyximport.install(inplace = True, language_level = 3, load_py_module_on_import_failure = True)

from core.gfx.effects.particle.particlesystem import ParticleSystem as Ps


__all__ = ("ParticleSystem", "DEFAULT", "HARDDROP_WIND", "HARDDROP_WIND_REVERSED", "SUN", "BURST")


class Emitter(enum.IntEnum):
	GRAVITY = 0
	RADIAL = 1


ParticleTexture = Image(join(dirname(__file__), "particle.png")).texture
ParticleTexture_Stick = Image(join(dirname(__file__), "particle2.png")).texture


DEFAULT = {'texture': ParticleTexture,
           'emitter_x_variance': 0,
           'emitter_y_variance': 0,
           'gravity_x': 0.0,
           'gravity_y': 0.0,
           'emitter_type': Emitter.GRAVITY,
           'max_num_particles': 100,
           'life_span': 0.01,
           'life_span_variance': 0.0,
           'start_size': 0.0,
           'start_size_variance': 0.0,
           'end_size': 0.0,
           'end_size_variance': 0.0,
           'emit_angle': 0.0,  # math.radians
           'emit_angle_variance': 0.0,  # math.radians
           'start_rotation': 0.0,  # math.radians
           'start_rotation_variance': 0.0,  # math.radians
           'end_rotation': 0.0,  # math.radians
           'end_rotation_variance': 0.0,  # math.radians
           'speed': 0.0,
           'speed_variance': 0.0,
           'radial_acceleration': 0.0,
           'radial_acceleration_variance': 0.0,
           'tangential_acceleration': 0.0,
           'tangential_acceleration_variance': 0.0,
           'max_radius': 0.0,
           'max_radius_variance': 0.0,
           'min_radius': 0.0,
           'rotate_per_second': 0.0,  # math.radians
           'rotate_per_second_variance': 0.0,  # math.radians
           'start_color': [1, 1, 1, 1],
           'start_color_variance': [1, 1, 1, 1],
           'end_color': [1, 1, 1, 1],
           'end_color_variance': [1, 1, 1, 1],
           'blend_factor_source': 0x302,
           'blend_factor_dest': 1}


SUN = {'texture': ParticleTexture,
       'emitter_x_variance': 0,
       'emitter_y_variance': 0,
       'gravity_x': 0.0,
       'gravity_y': 0.0,
       'emitter_type': Emitter.GRAVITY,
       'max_num_particles': 20,
       'life_span': 1.0,
       'life_span_variance': 0.7,
       'start_size': 60.0,
       'start_size_variance': 40.0,
       'end_size': 5.0,
       'end_size_variance': 5.0,
       'emit_angle': 0.0,  # math.radians
       'emit_angle_variance': 360.0,  # math.radians
       'start_rotation': 0.0,  # math.radians
       'start_rotation_variance': 0.0,  # math.radians
       'end_rotation': 0.0,  # math.radians
       'end_rotation_variance': 0.0,  # math.radians
       'speed': 260.0,
       'speed_variance': 10.0,
       'radial_acceleration': -380.0,
       'radial_acceleration_variance': 0.0,
       'tangential_acceleration': -140.0,
       'tangential_acceleration_variance': 0.0,
       'max_radius': 100.0,
       'max_radius_variance': 0.0,
       'min_radius': 0.0,
       'rotate_per_second': 0.0,  # math.radians
       'rotate_per_second_variance': 0.0,  # math.radians
       'start_color': [1, 0, 0, 1],
       'start_color_variance': [0, 0, 0, 0],
       'end_color': [1, 1, 0, 1],
       'end_color_variance': [0, 0, 0, 0],
       'blend_factor_source': 0x302,
       'blend_factor_dest': 1}


BURST = {'texture': ParticleTexture_Stick,
       'emitter_x_variance': 0,
       'emitter_y_variance': 0,
       'gravity_x': 0.0,
       'gravity_y': 0.0,
       'emitter_type': Emitter.GRAVITY,
       'max_num_particles': 200,
       'life_span': 1.0,
       'life_span_variance': 0.7,
       'start_size': 60.0,
       'start_size_variance': 40.0,
       'end_size': 5.0,
       'end_size_variance': 5.0,
       'emit_angle': 0.0,  # math.radians
       'emit_angle_variance': 360.0,  # math.radians
       'sync_emit_angle_rotation': True,
       'start_rotation': 0.0,  # math.radians
       'start_rotation_variance': 0.0,  # math.radians
       'end_rotation': 0.0,  # math.radians
       'end_rotation_variance': 0.0,  # math.radians
       'speed': 100.0,
       'speed_variance': 50.0,
       'radial_acceleration': 0.0,
       'radial_acceleration_variance': 0.0,
       'tangential_acceleration': 0.0,
       'tangential_acceleration_variance': 0.0,
       'max_radius': 360.0,
       'max_radius_variance': 360.0,
       'min_radius': 0.0,
       'rotate_per_second': 0.0,  # math.radians
       'rotate_per_second_variance': 0.0,  # math.radians
       'start_color': [1, 1, 1, 1],
       'start_color_variance': [1, 1, 1, 0],
       'end_color': [1, 0, 0, 1],
       'end_color_variance': [0, 0, 0, 0],
       'blend_factor_source': 0x302,
       'blend_factor_dest': 1}


HARDDROP_WIND = {'texture': ParticleTexture_Stick,
       'emitter_x_variance': 20,
       'emitter_y_variance': 0,
       'gravity_x': 0.0,
       'gravity_y': 0.0,
       'emitter_type': Emitter.GRAVITY,
       'max_num_particles': 20,
       'life_span': 0.4,
       'life_span_variance': 0.3,
       'start_size': 60.0,
       'start_size_variance': 40.0,
       'end_size': 5.0,
       'end_size_variance': 5.0,
       'emit_angle': 90.0,  # math.radians
       'emit_angle_variance': 0.0,  # math.radians
       'sync_emit_angle_rotation': False,
       'start_rotation': 90.0,  # math.radians
       'start_rotation_variance': 0.0,  # math.radians
       'end_rotation': 90.0,  # math.radians
       'end_rotation_variance': 0.0,  # math.radians
       'speed': 300.0,
       'speed_variance': 100.0,
       'radial_acceleration': 0.0,
       'radial_acceleration_variance': 0.0,
       'tangential_acceleration': 0.0,
       'tangential_acceleration_variance': 0.0,
       'max_radius': 0.0,
       'max_radius_variance': 0.0,
       'min_radius': 0.0,
       'rotate_per_second': 0.0,  # math.radians
       'rotate_per_second_variance': 0.0,  # math.radians
       'start_color': [1, 1, 1, 1],
       'start_color_variance': [1, 1, 1, 0],
       'end_color': [1, 0, 0, 1],
       'end_color_variance': [0, 0, 0, 0],
       'blend_factor_source': 0x302,
       'blend_factor_dest': 1}


HARDDROP_WIND_REVERSED = HARDDROP_WIND | {'emit_angle': 270, 'start_rotation': 270, 'end_rotation': 270}


class ParticleSystem(Ps):
	__slots__ = ()

	def __init__(self, config: dict, **k): super().__init__(DEFAULT | config, **k)
