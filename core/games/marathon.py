from core.engine import Engine
from core.games.games import Game
from core.misc import EndCondition


__all__ = "MarathonEngine", "MarathonGame"


class MarathonEngine(Engine):
	"""Marathon Game engine"""

	def get_end_condition(self) -> EndCondition:
		return EndCondition.Victory if self._level > 15 else EndCondition.Continue


class MarathonGame(Game):
	"""Marathon Game"""

	name: str = "Marathon"
	description: str = "Franchit le niveau 15 pour décrocher la victoire !"

	allow_config_random_tetrimino: bool = True
	allow_config_reversed_matrix: bool = True
	allow_config_starting_level: bool = True
	allow_config_variable_goal: bool = True
	allow_config_cycle_matrix: bool = True

	engine = MarathonEngine
