from core.engine import Engine
from core.misc import LockdownMode, PlayerSave, sec2time

from kivy.logger import Logger
from typing import Callable


__all__ = "Game", "GameResult"


class MetaGame(type):
	def __iter__(cls) -> iter:
		return iter(getattr(cls, "_games", ()))

	def __len__(cls) -> int:
		return len(getattr(cls, "_games", ()))

	def __getattribute__(cls, item):
		i = type.__getattribute__(cls, item)
		return getattr(PlayerSave, item) if i is Nothing else i


class Nothing: pass  # Alternative to None


class GameResult:
	"""Represents an important information about an ended game, shown on the end screen"""

	__slots__ = "key", "name", "default", "getter", "sort_key", "form"

	def __init__(self, key: str, name: str, default: str | int, getter: Callable = None, sort_key: Callable = None, form: Callable = None):
		"""
		Args:
			key (str): the key used as identifier in the save file
			name (str): the type name of the information shown (e.g. : "Score")
			default (str | int): used to replace an invalid or missing data
			getter (Callable): a callback taking the engine as argument, used by the end screen
			to retrieve information. By default, a getattr is performed on engine using the key
			sort_key (Callable): used to sort informations, passed to sorted(key =)
			form (Callable): used to format the info representation, take the info as arg and return a formatted value
		"""

		self.key = key
		self.name = name
		self.default = default
		self.getter = getter or (lambda engine: getattr(engine, key, default))
		self.sort_key = sort_key or (lambda x: x)
		self.form = form or (lambda x: x)

	def __repr__(self) -> str:
		return f"<GameResult key = {self.key} | name = {self.name} | default = {self.default}>"




class Game(metaclass = MetaGame):
	"""Represents a game mode, with all its rules"""

	_games = []

	# Meta
	name: str = ""
	description: str = ""
	max_player_num: int = 1
	player_num: int = 1

	# Result Classification
	rank_result = [GameResult("score", "Score", 0),
	               GameResult("time", "Temps", 0, sort_key = lambda t: -t, form = sec2time),
	               GameResult("cleared_lines", "Lignes", 0),
	               GameResult("level", "Niveau", 1)]

	# Allowed configurable elements
	allow_config_random_tetrimino: bool = False
	allow_config_reversed_matrix: bool = False
	allow_config_starting_level: bool = False
	allow_config_variable_goal: bool = False
	allow_config_cycle_matrix: bool = False

	# Engine Config
	is_hold_allowed: bool | type = Nothing
	lockdown_mode: LockdownMode | type = Nothing
	starting_level: int = 1

	use_level_up: bool = True
	line_goal: int | None = None

	use_chrono: bool = False
	time: int = 0

	use_random_tetrimino: bool = False
	use_reversed_matrix:  bool = False
	use_attack_column:    bool = False
	use_variable_goal:    bool = True
	use_cycle_matrix:     bool = False

	is_pause_allowed: bool = True
	is_multiplayer: bool = False
	is_online: bool = False

	random_key: bytes | int = None

	engine: type = Engine

	def __init_subclass__(cls):
		cls._games.append(cls)
		Logger.info(f"Game mode {cls.name} loaded")

	def __call__(self): return self

	def __iter__(self) -> iter:
		return iter(getattr(self, "_games", ()))

	def __len__(self) -> int:
		return len(getattr(self, "_games", ()))

	def __getattribute__(self, item):
		try: i = super().__getattribute__(item)
		except AttributeError: i = Nothing
		return getattr(PlayerSave, item) if i is Nothing else i

	def get(self, item: str, default = None):
		return getattr(self, item, default)
