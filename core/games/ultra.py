from core.engine import Engine
from core.games.games import Game
from core.misc import EndCondition


__all__ = "UltraEngine", "UltraGame"


class UltraEngine(Engine):
	"""Ultra Game engine"""

	def get_end_condition(self) -> EndCondition:
		return EndCondition.Victory if self.time <= 0 else EndCondition.Continue


class UltraGame(Game):
	"""Ultra Game"""

	name: str = "Ultra"
	description: str = "Détruit le maximum de lignes en 2 minutes !"

	allow_config_random_tetrimino: bool = True
	allow_config_reversed_matrix: bool = True
	allow_config_starting_level: bool = True
	allow_config_variable_goal: bool = False
	allow_config_cycle_matrix: bool = True

	use_level_up: bool = False
	use_chrono: bool = True
	time: int = 120

	engine = UltraEngine
