from core.engine import Engine
from core.games.games import Game
from core.misc import EndCondition


__all__ = "SprintEngine", "SprintGame"


class SprintEngine(Engine):
	"""Sprint Game engine"""

	def get_end_condition(self) -> EndCondition:
		return EndCondition.Victory if self.cleared_lines >= 40 else EndCondition.Continue


class SprintGame(Game):
	"""Sprint Game"""

	name: str = "Sprint"
	description: str = "Détruit 40 lignes le plus rapidement possible !"

	allow_config_random_tetrimino: bool = True
	allow_config_reversed_matrix: bool = True
	allow_config_starting_level: bool = True
	allow_config_variable_goal: bool = False
	allow_config_cycle_matrix: bool = True

	use_level_up: bool = False
	line_goal: int | None = 40

	engine = SprintEngine
