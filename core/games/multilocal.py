from core.engine import Engine
from core.games.games import Game, GameResult
from core.misc import EndCondition, LockdownMode


__all__ = "MultiLocalEngine", "MultiLocalGame"


class MultiLocalEngine(Engine):
	"""MultiLocal Game engine"""

	def get_end_condition(self) -> EndCondition:
		return EndCondition.Victory if not self.server.get_alive_opponent(self.player_num) else EndCondition.Continue


class MultiLocalGame(Game):
	"""MultiLocal Game"""

	name: str = "Multilocal"
	description: str = "Défiez vos amis dans ce mode multijoueur local ! Qui sera le dernier ?"
	max_player_num: int = 8

	player_num = 2

	rank_result = [GameResult("rank", "Rang", 1, sort_key = lambda r: -r, form = lambda r: f"1er" if r == 1 else f"{r}e"),
	               GameResult("score", "Score", 0),
	               GameResult("cleared_lines", "Lignes", 0),
	               GameResult("level", "Niveau", 1)]

	allow_config_random_tetrimino: bool = True
	allow_config_reversed_matrix: bool = True
	allow_config_starting_level: bool = True
	allow_config_variable_goal: bool = True
	allow_config_cycle_matrix: bool = True

	lockdown_mode: LockdownMode = LockdownMode.ExtendedPlacement

	use_attack_column: bool = True
	is_pause_allowed: bool = False
	is_ghost_allowed: bool = True
	is_hold_allowed: bool = True
	is_multiplayer: bool = True

	engine = MultiLocalEngine
